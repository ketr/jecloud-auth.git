/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.check.RbacInterface;
import com.je.auth.check.SystemVariablesInterface;
import com.je.auth.impl.AuthTempDefaultImpl;
import com.je.auth.impl.AuthTokenDaoDefaultImpl;
import com.je.auth.impl.DefaultAuthLoginManagerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public AuthTokenDao authTokenDao() {
        return new AuthTokenDaoDefaultImpl();
    }

    @Bean
    public RbacInterface rbacInterface() {
        return new RbacInterfaceImpl();
    }

    @Bean
    public SystemVariablesInterface systemVariablesInterface() {
        return new SystemVariablesInterfaceImpl();
    }

    @Bean
    public AuthTempInterface authTempInterface() {
        return new AuthTempDefaultImpl();
    }

    @Bean
    public AuthLoginManager authManager() {
        return new DefaultAuthLoginManagerImpl();
    }

    @Bean
    public OAuth2Template oAuth2Template() {
        return new OAuth2TemplateImpl();
    }

    @Bean
    public OAuth2Handle oAuth2Handle() {
        return new OAuth2HandleImpl();
    }

}
