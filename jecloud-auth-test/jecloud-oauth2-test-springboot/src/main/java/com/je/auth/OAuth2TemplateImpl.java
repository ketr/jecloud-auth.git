/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.model.ClientModel;

public class OAuth2TemplateImpl implements OAuth2Template {

    private OAuth2Engine oAuth2Engine;

    @Override
    public OAuth2Engine getoAuth2Engine() {
        return oAuth2Engine;
    }

    @Override
    public void setoAuth2Engine(OAuth2Engine oAuth2Engine) {
        this.oAuth2Engine = oAuth2Engine;
    }

    @Override
    public ClientModel getClientModel(String clientId) {
        // 此为模拟数据，真实环境需要从数据库查询
        if ("10001".equals(clientId)) {
            return new ClientModel()
                    .setClientId("10001")
                    .setClientSecret("aaaa-bbbb-cccc-dddd-eeee")
                    .setAllowUrl("*")
                    .setContractScope("userInfo")
                    .setIsAutoMode(true)
                    .setAccessTokenTimeout(oAuth2Engine.getoAuth2Config().getAccessTokenTimeout())
                    .setRefreshTokenTimeout(oAuth2Engine.getoAuth2Config().getRefreshTokenTimeout())
                    .setClientTokenTimeout(oAuth2Engine.getoAuth2Config().getClientTokenTimeout())
                    .setPastClientTokenTimeout(oAuth2Engine.getoAuth2Config().getPastClientTokenTimeout());
        }
        return null;
    }

    @Override
    public String getOpenid(String clientId, Object loginId) {
        return "gr_SwoIN0MC1ewxHX_vfCW3BothWDZMMtx__";
    }

}
