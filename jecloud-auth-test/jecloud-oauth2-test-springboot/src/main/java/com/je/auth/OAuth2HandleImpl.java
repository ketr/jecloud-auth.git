/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.config.OAuth2Config;
import com.je.auth.check.context.model.AuthRequest;
import com.je.auth.check.context.model.AuthResponse;
import com.je.auth.util.AuthResult;

public class OAuth2HandleImpl implements OAuth2Handle {

    private AuthEngine authEngine;

    private OAuth2Engine oAuth2Engine;

    @Override
    public AuthEngine getAuthEngine() {
        return authEngine;
    }

    @Override
    public void setAuthEngine(AuthEngine authEngine) {
        this.authEngine = authEngine;
    }

    @Override
    public OAuth2Engine getOAuth2Engine() {
        return oAuth2Engine;
    }

    @Override
    public void setOAuth2Engine(OAuth2Engine oAuth2Engine) {
        this.oAuth2Engine = oAuth2Engine;
    }

    @Override
    public AuthResult doLogin(AuthRequest req, AuthResponse res, OAuth2Config cfg) {
        String username = req.getParamNotNull("username");
        String password = req.getParamNotNull("password");
        String device = req.getParamNotNull("device");

        if ("10001".equals(username) && "123456".equals(password)) {
            authEngine.getAuthLoginManager().login(username, device);
        }

        return AuthResult.ok().setMsg("登录成功！");
    }

}
