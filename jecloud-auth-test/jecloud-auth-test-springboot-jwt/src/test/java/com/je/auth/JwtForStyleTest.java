/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.common.auth.token.AuthSession;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;

/**
 * 整合 jwt：Style 模式 测试
 *
 * @author kong
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartUpApplication.class)
public class JwtForStyleTest {

    // 持久化Bean
    @Autowired
    private AuthTokenDao dao;
    @Autowired
    private AuthLoginManager authManager;

    // 开始
    @BeforeClass
    public static void beforeClass() {
        System.out.println("\n\n------------------------ JwtForStyleTest start ...");
    }

    // 结束
    @AfterClass
    public static void afterClass() {
        System.out.println("\n\n------------------------ JwtForStyleTest end ... \n");
    }

    // 测试：登录 
    @Test
    public void doLogin() {
        // 登录
        authManager.login(10001);
        String token = authManager.getTokenValue();

        // API 验证
        Assert.assertTrue(authManager.isLogin());
        Assert.assertNotNull(token);    // token不为null
        Assert.assertEquals(authManager.getLoginIdAsLong(), 10001);    // loginId=10001
        Assert.assertEquals(authManager.getLoginDevice(), TokenConsts.DEFAULT_LOGIN_DEVICE);    // 登录设备

        // token 验证
        JWT jwt = JWT.of(token);
        JSONObject payloads = jwt.getPayloads();
        Assert.assertEquals(payloads.getStr("loginId"), "10001");

        // db数据 验证
        // token存在
        Assert.assertEquals(dao.get("authorization:token:" + token), "10001");
        // Session 存在
        AuthSession session = dao.getSession("authorization:session:" + 10001);
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getId(), "authorization:session:" + 10001);
        Assert.assertTrue(session.getTokenSignList().size() >= 1);
    }

}
