/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import java.util.List;

import com.je.auth.check.AuthCheckManager;
import com.je.auth.check.exception.ApiDisabledException;
import com.je.auth.check.exception.DisableLoginException;
import com.je.auth.util.JwtUtil;
import com.je.common.auth.token.AuthSession;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;

/**
 * 整合 jwt：mix 模式 测试
 *
 * @author kong
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartUpApplication.class)
public class JwtForMixTest {

	// 持久化Bean 
	@Autowired
	private AuthTokenDao dao;
	@Autowired
	private AuthLoginManager authLoginManager;
	@Autowired
	private AuthCheckManager authCheckManager;
	@Autowired
	private AuthSessionTemplate authSessionTemplate;

	// 开始 
	@BeforeClass
	public static void beforeClass() {
		System.out.println("\n\n------------------------ JwtForMixTest start ...");
	}

	// 结束 
	@AfterClass
	public static void afterClass() {
		System.out.println("\n\n------------------------ JwtForMixTest end ... \n");
	}

	// 测试：登录
	@Test
	public void doLogin() {
		// 登录
		authLoginManager.login(10001);
		String token = authLoginManager.getTokenValue();

		// API 验证
		Assert.assertTrue(authLoginManager.isLogin());
		Assert.assertNotNull(token);    // token不为null
		Assert.assertEquals(authLoginManager.getLoginIdAsLong(), 10001);    // loginId=10001
		Assert.assertEquals(authLoginManager.getLoginDevice(), TokenConsts.DEFAULT_LOGIN_DEVICE);    // 登录设备

		// token 验证
		JWT jwt = JWT.of(token);
		JSONObject payloads = jwt.getPayloads();
		Assert.assertEquals(payloads.getStr(JwtUtil.LOGIN_ID), "10001"); // 账号
		Assert.assertEquals(payloads.getStr(JwtUtil.DEVICE), TokenConsts.DEFAULT_LOGIN_DEVICE);  // 登录设备

		// db数据 验证
		// token不存在
		Assert.assertNull(dao.get("authorization:token:" + token));
		// Session 存在
		AuthSession session = dao.getSession("authorization:session:" + 10001);
		Assert.assertNotNull(session);
		Assert.assertEquals(session.getId(), "authorization:session:" + 10001);
		Assert.assertTrue(session.getTokenSignList().size() >= 1);
	}

	// 测试：注销
	@Test
	public void logout() {
		// 登录
		authLoginManager.login(10001);
		String token = authLoginManager.getTokenValue();
		Assert.assertEquals(JWT.of(token).getPayloads().getStr("loginId"), "10001");

		// 注销
		authLoginManager.logout();
		// token 应该被清除
		Assert.assertNull(authLoginManager.getTokenValue());
		Assert.assertFalse(authLoginManager.isLogin());
	}

	// 测试：Session会话
	@Test
	public void testSession() {
		authLoginManager.login(10001);

		// API 应该可以获取 Session
		Assert.assertNotNull(authLoginManager.getSession(false));

		// db中应该存在 Session
		AuthSession session = dao.getSession("authorization:session:" + 10001);
		Assert.assertNotNull(session);

		// 存取值
		authSessionTemplate.set(session, "name", "zhang");
		authSessionTemplate.set(session, "age", "18");

		Assert.assertEquals(authSessionTemplate.get(session, "name"), "zhang");
		Assert.assertEquals(authSessionTemplate.getInt(session, "age"), 18);
		Assert.assertEquals((int) authSessionTemplate.getModel(session, "age", int.class), 18);
		Assert.assertEquals((int) authSessionTemplate.get(session, "age", 20), 18);
		Assert.assertEquals((int) authSessionTemplate.get(session, "name2", 20), 20);
		Assert.assertEquals((int) authSessionTemplate.get(session, "name2", () -> 30), 30);
		authSessionTemplate.clear(session);

		Assert.assertEquals(authSessionTemplate.get(session, "name"), null);
	}

	// 测试：权限认证
	@Test
	public void testCheckPermission() {
		authLoginManager.login(10001);

		// 权限认证
		Assert.assertTrue(authCheckManager.hasPermission("user-add"));
		Assert.assertTrue(authCheckManager.hasPermission("user-list"));
		Assert.assertTrue(authCheckManager.hasPermission("user"));
		Assert.assertTrue(authCheckManager.hasPermission("art-add"));
		Assert.assertFalse(authCheckManager.hasPermission("get-user"));
		// and
		Assert.assertTrue(authCheckManager.hasPermissionAnd("art-add", "art-get"));
		Assert.assertFalse(authCheckManager.hasPermissionAnd("art-add", "comment-add"));
		// or
		Assert.assertTrue(authCheckManager.hasPermissionOr("art-add", "comment-add"));
		Assert.assertFalse(authCheckManager.hasPermissionOr("comment-add", "comment-delete"));
	}

	// 测试：角色认证
	@Test
	public void testCheckRole() {
		authLoginManager.login(10001);

		// 角色认证
		Assert.assertTrue(authCheckManager.hasRole("admin"));
		Assert.assertFalse(authCheckManager.hasRole("teacher"));
		// and
		Assert.assertTrue(authCheckManager.hasRoleAnd("admin", "super-admin"));
		Assert.assertFalse(authCheckManager.hasRoleAnd("admin", "ceo"));
		// or
		Assert.assertTrue(authCheckManager.hasRoleOr("admin", "ceo"));
		Assert.assertFalse(authCheckManager.hasRoleOr("ceo", "cto"));
	}

	// 测试：根据token强制注销
	@Test(expected = ApiDisabledException.class)
	public void testLogoutByToken() {

		// 先登录上
		authLoginManager.login(10001);
		Assert.assertTrue(authLoginManager.isLogin());
		String token = authLoginManager.getTokenValue();

		// 根据token注销
		authLoginManager.logoutByTokenValue(token);
	}

	// 测试：根据账号id强制注销
	@Test(expected = ApiDisabledException.class)
	public void testLogoutByLoginId() {

		// 先登录上
		authLoginManager.login(10001);
		Assert.assertTrue(authLoginManager.isLogin());

		// 根据账号id注销
		authLoginManager.logout(10001);
	}

	// 测试Token-Session
	@Test
	public void testTokenSession() {

		// 先登录上
		authLoginManager.login(10001);
		String token = authLoginManager.getTokenValue();

		// 刚开始不存在
		Assert.assertNull(authLoginManager.getTokenSession(false));
		AuthSession session = dao.getSession("authorization:token-session:" + token);
		Assert.assertNull(session);

		// 调用一次就存在了
		authLoginManager.getTokenSession();
		Assert.assertNotNull(authLoginManager.getTokenSession(false));
		AuthSession session2 = dao.getSession("authorization:token-session:" + token);
		Assert.assertNotNull(session2);
	}

	// 测试：账号封禁
	@Test(expected = DisableLoginException.class)
	public void testDisable() {

		// 封号
		authLoginManager.disable(10007, 200);
		Assert.assertTrue(authLoginManager.isDisable(10007));
		Assert.assertEquals(dao.get("authorization:disable:" + 10007), DisableLoginException.BE_VALUE);

		// 解封
		authLoginManager.untieDisable(10007);
		Assert.assertFalse(authLoginManager.isDisable(10007));
		Assert.assertEquals(dao.get("authorization:disable:" + 10007), null);

		// 封号后登陆 (会抛出 DisableLoginException 异常)
		authLoginManager.disable(10007, 200);
		authLoginManager.login(10007);
	}

	// 测试：身份切换
	@Test
	public void testSwitch() {
		// 登录
		authLoginManager.login(10001);
		Assert.assertFalse(authLoginManager.isSwitch());
		Assert.assertEquals(authLoginManager.getLoginIdAsLong(), 10001);

		// 开始身份切换
		authLoginManager.switchTo(10044);
		Assert.assertTrue(authLoginManager.isSwitch());
		Assert.assertEquals(authLoginManager.getLoginIdAsLong(), 10044);

		// 结束切换
		authLoginManager.endSwitch();
		Assert.assertFalse(authLoginManager.isSwitch());
		Assert.assertEquals(authLoginManager.getLoginIdAsLong(), 10001);
	}

	// 测试：会话管理
	@Test(expected = ApiDisabledException.class)
	public void testSearchTokenValue() {
		// 登录
		authLoginManager.login(10001);
		authLoginManager.login(10002);
		authLoginManager.login(10003);
		authLoginManager.login(10004);
		authLoginManager.login(10005);

		// 查询
		List<String> list = authLoginManager.searchTokenValue("", 0, 10);
		Assert.assertTrue(list.size() >= 5);
	}

}
