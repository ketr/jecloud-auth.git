/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.check.AuthCheckManager;
import com.je.auth.check.exception.ApiDisabledException;
import com.je.auth.util.JwtUtil;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;

/**
 * 整合 jwt：stateless 模式 测试
 *
 * @author kong
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartUpApplication.class)
public class JwtForStatelessTest {

    @Autowired
    private AuthLoginManager authLoginManager;
    @Autowired
    private AuthCheckManager authCheckManager;

    // 开始
    @BeforeClass
    public static void beforeClass() {
        System.out.println("\n\n------------------------ JwtForStatelessTest start ...");
    }

    // 结束
    @AfterClass
    public static void afterClass() {
        System.out.println("\n\n------------------------ JwtForStatelessTest end ... \n");
    }

    // 测试：登录 
    @Test
    public void doLogin() {
        // 登录
        authLoginManager.login(10001);
        String token = authLoginManager.getTokenValue();

        // API 验证
        Assert.assertTrue(authLoginManager.isLogin());
        Assert.assertNotNull(token);    // token不为null
        Assert.assertEquals(authLoginManager.getLoginIdAsLong(), 10001);    // loginId=10001
        Assert.assertEquals(authLoginManager.getLoginDevice(), TokenConsts.DEFAULT_LOGIN_DEVICE);    // 登录设备

        // token 验证
        JWT jwt = JWT.of(token);
        JSONObject payloads = jwt.getPayloads();
        Assert.assertEquals(payloads.getStr(JwtUtil.LOGIN_ID), "10001"); // 账号
        Assert.assertEquals(payloads.getStr(JwtUtil.DEVICE), TokenConsts.DEFAULT_LOGIN_DEVICE);  // 登录设备

        // 时间
        Assert.assertTrue(authLoginManager.getTokenTimeout() <= authLoginManager.getConfig().getTimeout());
        Assert.assertTrue(authLoginManager.getTokenTimeout() > authLoginManager.getConfig().getTimeout() - 10000);

        try {
            // 尝试获取Session会抛出异常
            authLoginManager.getSession();
            Assert.assertTrue(false);
        } catch (Exception e) {
        }
    }

    // 测试：注销 
    @Test
    public void logout() {
        // 登录
        authLoginManager.login(10001);
        String token = authLoginManager.getTokenValue();
        Assert.assertEquals(JWT.of(token).getPayloads().getStr("loginId"), "10001");

        // 注销
        authLoginManager.logout();

        // token 应该被清除
        Assert.assertNull(authLoginManager.getTokenValue());
        Assert.assertFalse(authLoginManager.isLogin());
    }

    // 测试：Session会话 
    @Test(expected = ApiDisabledException.class)
    public void testSession() {
        authLoginManager.login(10001);

        // 会抛异常
        authLoginManager.getSession();
    }

    // 测试：权限认证 
    @Test
    public void testCheckPermission() {
        authLoginManager.login(10001);

        // 权限认证
        Assert.assertTrue(authCheckManager.hasPermission("user-add"));
        Assert.assertTrue(authCheckManager.hasPermission("user-list"));
        Assert.assertTrue(authCheckManager.hasPermission("user"));
        Assert.assertTrue(authCheckManager.hasPermission("art-add"));
        Assert.assertFalse(authCheckManager.hasPermission("get-user"));
        // and
        Assert.assertTrue(authCheckManager.hasPermissionAnd("art-add", "art-get"));
        Assert.assertFalse(authCheckManager.hasPermissionAnd("art-add", "comment-add"));
        // or
        Assert.assertTrue(authCheckManager.hasPermissionOr("art-add", "comment-add"));
        Assert.assertFalse(authCheckManager.hasPermissionOr("comment-add", "comment-delete"));
    }

    // 测试：角色认证
    @Test
    public void testCheckRole() {
        authLoginManager.login(10001);

        // 角色认证
        Assert.assertTrue(authCheckManager.hasRole("admin"));
        Assert.assertFalse(authCheckManager.hasRole("teacher"));
        // and
        Assert.assertTrue(authCheckManager.hasRoleAnd("admin", "super-admin"));
        Assert.assertFalse(authCheckManager.hasRoleAnd("admin", "ceo"));
        // or
        Assert.assertTrue(authCheckManager.hasRoleOr("admin", "ceo"));
        Assert.assertFalse(authCheckManager.hasRoleOr("ceo", "cto"));
    }

    // 测试：根据token强制注销 
    @Test(expected = ApiDisabledException.class)
    public void testLogoutByToken() {

        // 先登录上
        authLoginManager.login(10001);
        Assert.assertTrue(authLoginManager.isLogin());
        String token = authLoginManager.getTokenValue();

        // 根据token注销
        authLoginManager.logoutByTokenValue(token);
    }

    // 测试：根据账号id强制注销 
    @Test(expected = ApiDisabledException.class)
    public void testLogoutByLoginId() {

        // 先登录上
        authLoginManager.login(10001);
        Assert.assertTrue(authLoginManager.isLogin());

        // 根据账号id注销
        authLoginManager.logout(10001);
    }

}
