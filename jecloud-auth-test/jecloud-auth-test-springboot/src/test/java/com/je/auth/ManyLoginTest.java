/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.config.TokenConfig;
import com.je.common.auth.token.TokenSign;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

/**
 * 多端登录测试
 *
 * @author kong
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StartUpApplication.class)
public class ManyLoginTest {

    @Autowired
    private AuthTokenDao dao;
    @Autowired
    private AuthLoginManager authManager;
    @Autowired
    private AuthEngine authEngine;

    // 开始
    @BeforeClass
    public static void beforeClass() {
        System.out.println("\n------------ 多端登录测试 start ...");
    }

    // 结束
    @AfterClass
    public static void afterClass() {
        System.out.println("\n---------- 多端登录测试 end ... \n");
    }

    // 测试：并发登录、共享token、同端 
    @Test
    public void login() {
        authEngine.setConfig(new TokenConfig());

        authManager.login(10001);
        String token1 = authManager.getTokenValue();

        authManager.login(10001);
        String token2 = authManager.getTokenValue();

        Assert.assertEquals(token1, token2);
    }

    // 测试：并发登录、共享token、不同端 
    @Test
    public void login2() {
        authEngine.setConfig(new TokenConfig());

        authManager.login(10001, "APP");
        String token1 = authManager.getTokenValue();

        authManager.login(10001, "PC");
        String token2 = authManager.getTokenValue();

        Assert.assertNotEquals(token1, token2);
    }

    // 测试：并发登录、不共享token
    @Test
    public void login3() {
        authEngine.setConfig(new TokenConfig().setIsShare(false));

        authManager.login(10001);
        String token1 = authManager.getTokenValue();

        authManager.login(10001);
        String token2 = authManager.getTokenValue();

        Assert.assertNotEquals(token1, token2);
    }

    // 测试：禁并发登录，后者顶出前者 
    @Test
    public void login4() {
        authEngine.setConfig(new TokenConfig().setIsConcurrent(false));

        authManager.login(10001);
        String token1 = authManager.getTokenValue();

        authManager.login(10001);
        String token2 = authManager.getTokenValue();

        // token不同
        Assert.assertNotEquals(token1, token2);

        // token1会被标记为：已被顶下线
        Assert.assertEquals(dao.get("authorization:token:" + token1), "-4");

        // User-Session里的 token1 签名会被移除
        List<TokenSign> tokenSignList = authManager.getSessionByLoginId(10001).getTokenSignList();
        for (TokenSign tokenSign : tokenSignList) {
            Assert.assertNotEquals(tokenSign.getValue(), token1);
        }
    }

    // 测试：多端登录，一起强制注销 
    @Test
    public void login5() {
        authEngine.setConfig(new TokenConfig());

        authManager.login(10001, "APP");
        String token1 = authManager.getTokenValue();

        authManager.login(10001, "PC");
        String token2 = authManager.getTokenValue();

        authManager.login(10001, "h5");
        String token3 = authManager.getTokenValue();

        // 注销
        authManager.logout(10001);

        // 三个Token应该全部无效
        Assert.assertNull(dao.get("authorization:token:" + token1));
        Assert.assertNull(dao.get("authorization:token:" + token2));
        Assert.assertNull(dao.get("authorization:token:" + token3));

        // User-Session也应该被清除掉
        Assert.assertNull(authManager.getSessionByLoginId(10001, false));
        Assert.assertNull(dao.getSession("authorization:session:" + 10001));
    }

    // 测试：多端登录，一起强制踢下线 
    @Test
    public void login6() {
        authEngine.setConfig(new TokenConfig());

        authManager.login(10001, "APP");
        String token1 = authManager.getTokenValue();

        authManager.login(10001, "PC");
        String token2 = authManager.getTokenValue();

        authManager.login(10001, "h5");
        String token3 = authManager.getTokenValue();

        // 注销
        authManager.kickout(10001);

        // 三个Token应该全部无效
        Assert.assertEquals(dao.get("authorization:token:" + token1), "-5");
        Assert.assertEquals(dao.get("authorization:token:" + token2), "-5");
        Assert.assertEquals(dao.get("authorization:token:" + token3), "-5");

        // User-Session也应该被清除掉
        Assert.assertNull(authManager.getSessionByLoginId(10001, false));
        Assert.assertNull(dao.getSession("authorization:session:" + 10001));
    }

}
