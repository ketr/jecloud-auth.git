/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.login.wechat;

import com.je.auth.AuthEngine;
import com.je.auth.check.context.model.AuthRequest;
import com.je.auth.check.context.model.AuthResponse;
import com.je.auth.util.AuthResult;

/**
 * Wechat单点登录处理定义
 */
public interface WechatOAuthHandle {

    AuthEngine getAuthEngine();

    void setAuthEngine(AuthEngine authEngine);

    WechatLoginEngine getWechatLoginEngine();

    void setWechatLoginEngine(WechatLoginEngine wechatLoginEngine);

    /**
     * 处理Server端请求， 路由分发
     *
     * @return 处理结果
     */
    default AuthResult serverRequest() throws WechatLoginException {
        // 获取变量
        AuthRequest req = getAuthEngine().getAuthCheckEngine().getAuthTokenContext().getRequest();
        AuthResponse res = getAuthEngine().getAuthCheckEngine().getAuthTokenContext().getResponse();

        return null;
    }



}
