/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.login.wechat.model;

/**
 * 微信登录常量表
 */
public class WechatLoginConsts {

    /**
     * 微信接口url定义
     */
    public static final class Url {
        /**
         * 认证地址
         */
        public static String authorizeUrl = "https://open.weixin.qq.com/connect/qrconnect";

        /**
         * 获取token地址
         */
        public static String requireTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token";

        /**
         * 刷新token地址
         */
        public static String refreshAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/refresh_token";

        /**
         * 检查access_token有效性url
         */
        public static String checkTokenValidUrl = "https://api.weixin.qq.com/sns/auth";

        /**
         * 获取用户个人信息
         */
        public static String getUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo";
    }
    
    /**
     * 所有API接口
     *
     * @author kong
     */
    public static final class Api {
        public static String authorize = "/wechat/authorize";
        public static String token = "/wechat/token";
        public static String refresh = "/oauth2/refresh";
        public static String doLogin = "/oauth2/doLogin";
        public static String doConfirm = "/oauth2/doConfirm";
    }

    /**
     * 所有参数名称
     *
     * @author kong
     */
    public static final class Param {
        public static String response_type = "response_type";
        public static String appid = "appid";
        public static String secret = "secret";
        public static String redirect_uri = "redirect_uri";
        public static String scope = "scope";
        public static String state = "state";
        public static String code = "code";
        public static String openid = "openid";
        public static String access_token = "access_token";
        public static String refresh_token = "refresh_token";
        public static String grant_type = "grant_type";
    }

    /**
     * 所有返回类型
     */
    public static final class ResponseType {
        /**
         * 返回授权码
         */
        public static String code = "code";
    }

    /**
     * 所有授权类型
     */
    public static final class GrantType {
        /**
         * 授权码类型
         */
        public static String authorization_code = "authorization_code";
    }

}
