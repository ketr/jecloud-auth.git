/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.config;

import com.je.auth.check.AuthCheckEngine;
import com.je.auth.check.AuthCheckManager;
import com.je.auth.check.RbacInterface;
import com.je.auth.check.SystemVariablesInterface;
import com.je.auth.check.config.CheckConfig;
import com.je.auth.check.context.AuthTokenContext;
import com.je.auth.check.impl.DefaultAuthCheckManager;
import com.je.auth.check.impl.SystemVariablesInterfaceDefaultImpl;
import com.je.auth.check.strategy.AuthCheckStrategy;
import com.je.auth.spring.AuthTokenContextForSpring;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthCheckEngineConfiguration {

    @Bean
    AuthCheckEngine authCheckEngine(CheckConfig checkConfig, AuthCheckManager authCheckManager, AuthTokenContext authTokenContext,
                                    AuthCheckStrategy authCheckStrategy, RbacInterface rbacInterface, SystemVariablesInterface systemVariablesInterface) {
        return new AuthCheckEngine(checkConfig, authTokenContext, authCheckManager, authCheckStrategy, rbacInterface, systemVariablesInterface);
    }

    @Bean
    @ConditionalOnMissingBean(SystemVariablesInterface.class)
    public SystemVariablesInterface systemVariablesInterface() {
        return new SystemVariablesInterfaceDefaultImpl();
    }

    @Bean
    @ConfigurationProperties(prefix = "jecloud.auth.check")
    CheckConfig checkConfig() {
        return new CheckConfig();
    }

    @Bean
    AuthCheckManager authCheckManager() {
        return new DefaultAuthCheckManager();
    }

    @Bean
    AuthCheckStrategy authCheckStrategy() {
        return new AuthCheckStrategy();
    }

    @Bean
    public AuthTokenContext authTokenContext() {
        return new AuthTokenContextForSpring();
    }
}
