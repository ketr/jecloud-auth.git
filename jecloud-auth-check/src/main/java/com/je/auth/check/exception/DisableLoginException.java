/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.exception;

import com.je.auth.check.exception.TokenException;

/**
 * 一个异常：代表账号已被封禁
 * 
 * @author kong
 */
public class DisableLoginException extends TokenException {

	/**
	 * 序列化版本号
	 */
	private static final long serialVersionUID = 6806129545290130143L;

	/** 异常标记值 */
	public static final String BE_VALUE = "disable";
	
	/** 异常提示语 */
	public static final String BE_MESSAGE = "此账号已被封禁";

	/**
	 * 被封禁的账号id 
	 */
	private Object loginId;
	
	/**
	 * 封禁剩余时间，单位：秒 
	 */
	private long disableTime;

	/**
	 * 获取: 被封禁的账号id 
	 * 
	 * @return See above
	 */
	public Object getLoginId() {
		return loginId;
	}
	
	/**
	 * 获取: 封禁剩余时间，单位：秒
	 * @return See above
	 */
	public long getDisableTime() {
		return disableTime;
	}

	/**
	 * 一个异常：代表账号已被封禁
	 *
	 * @param loginId     被封禁的账号id
	 * @param disableTime 封禁剩余时间，单位：秒
	 */
	public DisableLoginException(Object loginId, long disableTime) {
		super(BE_MESSAGE);
		this.loginId = loginId;
		this.disableTime = disableTime;
	}

}
