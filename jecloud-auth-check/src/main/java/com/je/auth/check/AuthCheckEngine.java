/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check;

import com.je.auth.check.config.CheckConfig;
import com.je.auth.check.context.AuthTokenContext;
import com.je.auth.check.strategy.AuthCheckStrategy;

/**
 * 认证校验引擎
 */
public class AuthCheckEngine {

    /**
     * check配置
     */
    private CheckConfig config;
    /**
     * check管理器
     */
    private AuthCheckManager authCheckManager;
    /**
     * 权限认证接口，实现此接口即可集成权限认证功能
     */
    private RbacInterface rbacInterface;
    /**
     * 权限认证接口，实现此接口即可集成权限认证功能
     */
    private SystemVariablesInterface systemVariablesInterface;
    /**
     * 校验策略，用户可以重写
     */
    private AuthCheckStrategy authCheckStrategy;
    /**
     * 路由检验
     */
    private AuthPathChecker authPathChecker = new AuthPathChecker(this);
    /**
     * 上下文处理器
     */
    private AuthTokenContext authTokenContext;

    public AuthCheckEngine(CheckConfig config,
                           AuthTokenContext authTokenContext,
                           AuthCheckManager authCheckManager,
                           AuthCheckStrategy authCheckStrategy,
                           RbacInterface rbacInterface,
                           SystemVariablesInterface systemVariablesInterface) {
        this.config = config;
        this.authTokenContext = authTokenContext;
        this.authTokenContext.setAuthCheckEngine(this);
        this.authCheckManager = authCheckManager;
        this.authCheckManager.setAuthCheckEngine(this);
        this.authCheckStrategy = authCheckStrategy;
        this.authCheckStrategy.setAuthCheckEngine(this);
        this.rbacInterface = rbacInterface;
        this.systemVariablesInterface = systemVariablesInterface;
    }

    public CheckConfig getConfig() {
        return config;
    }

    public AuthTokenContext getAuthTokenContext() {
        return authTokenContext;
    }

    public AuthCheckManager getAuthCheckManager() {
        return authCheckManager;
    }

    public RbacInterface getRbacInterface() {
        return rbacInterface;
    }

    public SystemVariablesInterface getSystemVariablesInterface() {
        return systemVariablesInterface;
    }

    public AuthCheckStrategy getAuthCheckStrategy() {
        return authCheckStrategy;
    }

    public AuthPathChecker getAuthPathChecker() {
        return authPathChecker;
    }


}
