/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.config;

public class CheckConfig {

    /**
     * 切面、拦截器、过滤器等各种组件的注册优先级顺序
     */
    public static final int ASSEMBLY_ORDER = -100;

    /**
     * 配置当前项目的网络访问地址
     */
    private String currDomain;
    /**
     * 是否尝试从请求体里读取token
     */
    private Boolean isReadBody = true;
    /**
     * 是否尝试从header里读取token
     */
    private Boolean isReadHead = true;
    /**
     * 是否尝试从cookie里读取token
     */
    private Boolean isReadCookie = true;

    /**
     * @return 配置当前项目的网络访问地址
     */
    public String getCurrDomain() {
        return currDomain;
    }

    /**
     * @param currDomain 配置当前项目的网络访问地址
     * @return 对象自身
     */
    public CheckConfig setCurrDomain(String currDomain) {
        this.currDomain = currDomain;
        return this;
    }

    /**
     * @return 是否尝试从请求体里读取token
     */
    public Boolean getIsReadBody() {
        return isReadBody;
    }

    /**
     * @param isReadBody 是否尝试从请求体里读取token
     * @return 对象自身
     */
    public CheckConfig setIsReadBody(Boolean isReadBody) {
        this.isReadBody = isReadBody;
        return this;
    }

    /**
     * @return 是否尝试从header里读取token
     */
    public Boolean getIsReadHead() {
        return isReadHead;
    }

    /**
     * @param isReadHead 是否尝试从header里读取token
     * @return 对象自身
     */
    public CheckConfig setIsReadHead(Boolean isReadHead) {
        this.isReadHead = isReadHead;
        return this;
    }

    /**
     * @return 是否尝试从cookie里读取token
     */
    public Boolean getIsReadCookie() {
        return isReadCookie;
    }

    /**
     * @param isReadCookie 是否尝试从cookie里读取token
     * @return 对象自身
     */
    public CheckConfig setIsReadCookie(Boolean isReadCookie) {
        this.isReadCookie = isReadCookie;
        return this;
    }

    @Override
    public String toString() {
        return "CheckConfig{" +
                "currDomain='" + currDomain + '\'' +
                ", isReadBody=" + isReadBody +
                ", isReadHead=" + isReadHead +
                ", isReadCookie=" + isReadCookie +
                '}';
    }
}
