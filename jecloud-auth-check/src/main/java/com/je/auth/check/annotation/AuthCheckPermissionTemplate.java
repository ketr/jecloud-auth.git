/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限认证：必须具有指定权限才能进入该方法 ，请注意，此方法只能在controller中使用
 * <p> 可标注在函数、类上（效果等同于标注在此类的所有方法上） 
 * @author liulijun
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface AuthCheckPermissionTemplate {

	/**
	 * 需要校验的权限模板
	 * @return 需要校验的权限码
	 */
	String[] template() default {};

	/**
	 * 参数，存在先后顺序
	 * @return
	 */
	String[] params() default {};

	/**
	 * 验证模式：AND | OR，默认AND
	 * @return 验证模式
	 */
	AuthMode mode() default AuthMode.AND;

	/**
	 * 在权限认证不通过时的次要选择，两者只要其一认证成功即可通过校验  
	 * 
	 * <p> 
	 * 	例1：@SaCheckPermission(value="user-add", orRole="admin")，
	 * 	代表本次请求只要具有 user-add权限 或 admin角色 其一即可通过校验 
	 * </p>
	 * 
	 * <p> 
	 * 	例2： orRole = {"admin", "manager", "staff"}，具有三个角色其一即可 <br> 
	 * 	例3： orRole = {"admin, manager, staff"}，必须三个角色同时具备 
	 * </p>
	 * 
	 * @return /
	 */
	String[] orRole() default {};
	
}
