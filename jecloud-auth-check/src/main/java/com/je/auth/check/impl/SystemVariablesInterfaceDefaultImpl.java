/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.impl;

import com.je.auth.check.SystemVariablesInterface;
import com.je.auth.check.context.model.AuthRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 对 {@link SystemVariablesInterface} 接口默认的实现类
 * <p>
 * 如果开发者没有实现StpInterface接口，则使用此默认实现
 *
 * @author kong
 */

public class SystemVariablesInterfaceDefaultImpl implements SystemVariablesInterface {

    @Override
    public Map<String, String> getSystemVariablesByCodes(String[] codes) {
        return new HashMap<>();
    }

    @Override
    public String getRequestFormData(AuthRequest authRequest, String code) {
        System.out.println("================");
        System.out.println("getRequestFormData默认方法获取数据为null，请更新meta项目sdk");
        return "";
    }

    @Override
    public String getRequestHeaderData(AuthRequest authRequest, String code) {
        return "";
    }

    @Override
    public List<String> getChildFuncCodesByFuncCode(String funcCode) {
        System.out.println("================");
        System.out.println("getRequestFormData默认方法获取数据为null，请更新meta项目sdk");
        return new ArrayList<>();
    }
}
