/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check;

import com.je.common.auth.AuthAccount;

import java.util.List;

/**
 * 权限认证接口，实现此接口即可集成权限认证功能
 *
 * @author kong
 */
public interface RbacInterface {

    /**
     * 是否登录
     * @return
     */
    boolean isLogin();

    /**
     * 获取登录ID
     * @return
     */
    String getLoginId();

    /**
     * 获取租户id
     * @return
     */
    String getTenantId();

    /**
     * 获取登录用户
     * @return
     */
    AuthAccount getLoginAccount();

    /**
     * 返回指定账号id所拥有的权限码集合
     *
     * @param loginId 账号id
     * @param tenantId 租户id
     * @return 该账号id具有的权限码集合
     */
    List<String> getPermissionList(Object loginId,String tenantId);

    /**
     * 返回指定账号id所拥有的角色标识集合
     *
     * @param loginId 账号id
     * @param tenantId
     * @return 该账号id具有的角色标识集合
     */
    List<String> getRoleList(Object loginId,String tenantId);

    /**
     * 返回指定账号id所拥有的部门标识的集合
     *
     * @param loginId
     * @return
     */
    List<String> getDeptList(Object loginId,String tenantId);

    /**
     * 返回指定账号id所拥有的机构标识的集合
     *
     * @return
     */
    List<String> getOrgList(Object loginId);

    String getPcFuncShowCode(String funcCode);

    List<String> getPcFuncButtonCodes(String funcCode, String[] buttonCodes);

}
