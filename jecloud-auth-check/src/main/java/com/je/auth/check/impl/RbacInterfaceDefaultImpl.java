/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.impl;

import com.je.auth.check.RbacInterface;
import com.je.common.auth.AuthAccount;

import java.util.ArrayList;
import java.util.List;

/**
 * 对 {@link RbacInterface} 接口默认的实现类
 * <p>
 * 如果开发者没有实现StpInterface接口，则使用此默认实现
 *
 * @author kong
 */
public class RbacInterfaceDefaultImpl implements RbacInterface {

    @Override
    public boolean isLogin() {
        return false;
    }

    @Override
    public String getLoginId() {
        return null;
    }

    @Override
    public String getTenantId() {
        return null;
    }

    @Override
    public AuthAccount getLoginAccount() {
        return null;
    }

    @Override
    public List<String> getPermissionList(Object loginId, String tenantId) {
        return new ArrayList<>();
    }

    @Override
    public List<String> getRoleList(Object loginId, String tenantId) {
        return new ArrayList<>();
    }

    @Override
    public List<String> getDeptList(Object loginId, String tenantId) {
        return new ArrayList<>();
    }

    @Override
    public List<String> getOrgList(Object loginId) {
        return new ArrayList<>();
    }

    @Override
    public String getPcFuncShowCode(String funcCode) {
        return "";
    }

    @Override
    public List<String> getPcFuncButtonCodes(String funcCode, String[] buttonCodes) {
        return new ArrayList<>();
    }


}
