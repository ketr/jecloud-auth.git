/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.router;

import com.je.auth.check.AuthPathChecker;
import com.je.auth.check.exception.BackResultException;
import com.je.auth.check.exception.StopMatchException;
import com.je.auth.check.fun.AuthFunction;
import com.je.auth.check.fun.AuthParamFunction;
import com.je.auth.check.fun.AuthParamRetFunction;

import java.util.List;

/**
 * 路由匹配操作对象
 *
 * @author kong
 */
public class AuthPathCheckStaff {

	private AuthPathChecker checker;

	public AuthPathCheckStaff(AuthPathChecker checker) {
		this.checker = checker;
	}

	/**
	 * 是否命中的标记变量
	 */
	public boolean isHit = true;

	/**
	 * @return 是否命中
	 */
	public boolean isHit() {
		return isHit;
	}

	/**
	 * @param isHit 命中标记
	 * @return 对象自身
	 */
	public AuthPathCheckStaff setHit(boolean isHit) {
		this.isHit = isHit;
		return this;
	}

	/**
	 * 重置命中标记为 true
	 *
	 * @return 对象自身
	 */
	public AuthPathCheckStaff reset() {
		this.isHit = true;
		return this;
	}

	// ----------------- path匹配 

	/**
	 * 路由匹配
	 *
	 * @param patterns 路由匹配符数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff match(String... patterns) {
		if (isHit) {
			isHit = checker.isMatchCurrURI(patterns);
		}
		return this;
	}

	/**
	 * 路由匹配排除
	 *
	 * @param patterns 路由匹配符排除数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff notMatch(String... patterns) {
		if (isHit) {
			isHit = !checker.isMatchCurrURI(patterns);
		}
		return this;
	}

	/**
	 * 路由匹配
	 *
	 * @param patterns 路由匹配符集合
	 * @return 对象自身
	 */
	public AuthPathCheckStaff match(List<String> patterns) {
		if (isHit) {
			isHit = checker.isMatchCurrURI(patterns);
		}
		return this;
	}

	/**
	 * 路由匹配排除
	 *
	 * @param patterns 路由匹配符排除集合
	 * @return 对象自身
	 */
	public AuthPathCheckStaff notMatch(List<String> patterns) {
		if(isHit) {
			isHit = !checker.isMatchCurrURI(patterns);
		}
		return this;
	}

	// ----------------- Method匹配 

	/**
	 * Http请求方法匹配 (Enum)
	 *
	 * @param methods Http请求方法断言数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff match(AuthHttpMethod... methods) {
		if (isHit) {
			isHit = checker.isMatchCurrMethod(methods);
		}
		return this;
	}

	/**
	 * Http请求方法匹配排除 (Enum)
	 *
	 * @param methods Http请求方法断言排除数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff notMatch(AuthHttpMethod... methods) {
		if (isHit) {
			isHit = !checker.isMatchCurrMethod(methods);
		}
		return this;
	}

	/**
	 * Http请求方法匹配 (String)
	 *
	 * @param methods Http请求方法断言数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff matchMethod(String... methods) {
		if (isHit) {
			AuthHttpMethod[] arr = AuthHttpMethod.toEnumArray(methods);
			isHit = checker.isMatchCurrMethod(arr);
		}
		return this;
	}

	/**
	 * Http请求方法匹配排除 (String)
	 *
	 * @param methods Http请求方法断言排除数组
	 * @return 对象自身
	 */
	public AuthPathCheckStaff notMatchMethod(String... methods) {
		if(isHit)  {
			AuthHttpMethod[] arr = AuthHttpMethod.toEnumArray(methods);
			isHit = !checker.isMatchCurrMethod(arr);
		}
		return this;
	}

	// ----------------- 条件匹配 

	/**
	 * 根据 boolean 值进行匹配 
	 * @param flag boolean值 
	 * @return 对象自身
	 */
	public AuthPathCheckStaff match(boolean flag) {
		if (isHit) {
			isHit = flag;
		}
		return this;
	}

	/**
	 * 根据 boolean 值进行匹配排除 
	 * @param flag boolean值 
	 * @return 对象自身
	 */
	public AuthPathCheckStaff notMatch(boolean flag) {
		if (isHit) {
			isHit = !flag;
		}
		return this;
	}

	/**
	 * 根据自定义方法进行匹配 (lazy)  
	 * @param fun 自定义方法
	 * @return 对象自身
	 */
	public AuthPathCheckStaff match(AuthParamRetFunction<Object, Boolean> fun) {
		if (isHit) {
			isHit = fun.run(this);
		}
		return this;
	}

	/**
	 * 根据自定义方法进行匹配排除 (lazy) 
	 * @param fun 自定义排除方法
	 * @return 对象自身 
	 */
	public AuthPathCheckStaff notMatch(AuthParamRetFunction<Object, Boolean> fun) {
		if (isHit) {
			isHit = !fun.run(this);
		}
		return this;
	}


	// ----------------- 函数校验执行

	/**
	 * 执行校验函数 (无参) 
	 * @param fun 要执行的函数 
	 * @return 对象自身
	 */
	public AuthPathCheckStaff check(AuthFunction fun) {
		if (isHit) {
			fun.run();
		}
		return this;
	}

	/**
	 * 执行校验函数 (带参) 
	 * @param fun 要执行的函数 
	 * @return 对象自身
	 */
	public AuthPathCheckStaff check(AuthParamFunction<AuthPathCheckStaff> fun) {
		if (isHit) {
			fun.run(this);
		}
		return this;
	}

	/**
	 * 自由匹配 （ 在free作用域里执行stop()不会跳出Auth函数，而是仅仅跳出free代码块 ）
	 *
	 * @param fun 要执行的函数
	 * @return 对象自身
	 */
	public AuthPathCheckStaff free(AuthParamFunction<AuthPathCheckStaff> fun) {
		if (isHit) {
			try {
				fun.run(this);
			} catch (StopMatchException e) {
				// 跳出 free自由匹配代码块 
			}
		}
		return this;
	}


	// ----------------- 直接指定check函数 

	/**
	 * 路由匹配，如果匹配成功则执行认证函数 
	 * @param pattern 路由匹配符
	 * @param fun 要执行的校验方法 
	 * @return /
	 */
	public AuthPathCheckStaff match(String pattern, AuthFunction fun) {
		return this.match(pattern).check(fun);
	}

	/**
	 * 路由匹配，如果匹配成功则执行认证函数 
	 * @param pattern 路由匹配符
	 * @param fun 要执行的校验方法 
	 * @return /
	 */
	public AuthPathCheckStaff match(String pattern, AuthParamFunction<AuthPathCheckStaff> fun) {
		return this.match(pattern).check(fun);
	}

	/**
	 * 路由匹配 (并指定排除匹配符)，如果匹配成功则执行认证函数 
	 * @param pattern 路由匹配符 
	 * @param excludePattern 要排除的路由匹配符 
	 * @param fun 要执行的方法 
	 * @return /
	 */
	public AuthPathCheckStaff match(String pattern, String excludePattern, AuthFunction fun) {
		return this.match(pattern).notMatch(excludePattern).check(fun);
	}

	/**
	 * 路由匹配 (并指定排除匹配符)，如果匹配成功则执行认证函数
	 *
	 * @param pattern        路由匹配符
	 * @param excludePattern 要排除的路由匹配符
	 * @param fun            要执行的方法
	 * @return /
	 */
	public AuthPathCheckStaff match(String pattern, String excludePattern, AuthParamFunction<AuthPathCheckStaff> fun) {
		return this.match(pattern).notMatch(excludePattern).check(fun);
	}


	// ----------------- 提前退出

	/**
	 * 停止匹配，跳出函数 (在多个匹配链中一次性跳出Auth函数)
	 *
	 * @return 对象自身
	 */
	public AuthPathCheckStaff stop() {
		if (isHit) {
			throw new StopMatchException();
		}
		return this;
	}

	/**
	 * 停止匹配，结束执行，向前端返回结果
	 *
	 * @return 对象自身
	 */
	public AuthPathCheckStaff back() {
		if (isHit) {
			throw new BackResultException("");
		}
		return this;
	}
	
	/**
	 * 停止匹配，结束执行，向前端返回结果 
	 * @return 对象自身 
	 * @param result 要输出的结果 
	 */
	public AuthPathCheckStaff back(Object result) {
		if(isHit) {
			throw new BackResultException(result);
		}
		return this;
	}

	
}
