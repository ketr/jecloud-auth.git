/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.exception;

import com.je.auth.check.util.FoxUtil;

/**
 * AUTH-Token框架内部逻辑发生错误抛出的异常
 * (自定义此异常方便开发者在做全局异常处理时分辨异常类型)
 *
 * @author kong
 */
public class TokenException extends RuntimeException {

    /**
     * 序列化版本号
     */
    private static final long serialVersionUID = 6806129545290130132L;

    /**
     * 构建一个异常
     *
     * @param message 异常描述信息
     */
    public TokenException(String message) {
        super(message);
    }

    /**
     * 构建一个异常
     *
     * @param cause 异常对象
     */
    public TokenException(Throwable cause) {
        super(cause);
    }

    /**
     * 构建一个异常
     *
     * @param message 异常信息
     * @param cause   异常对象
     */
    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 如果flag==true，则抛出message异常
     *
     * @param flag    标记
     * @param message 异常信息
     */
    public static void throwBy(boolean flag, String message) {
        if (flag) {
            throw new TokenException(message);
        }
    }

    /**
     * 如果value==null或者isEmpty，则抛出message异常
     *
     * @param value   值
     * @param message 异常信息
     */
    public static void throwByNull(Object value, String message) {
        if (FoxUtil.isEmpty(value)) {
            throw new TokenException(message);
        }
    }

}
