/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.fun;

/**
 * 根据Boolean变量，决定是否执行一个函数
 *
 * @author kong
 */
public class IsRunFunction {

    /**
     * 变量
     */
    public final Boolean isRun;

    /**
     * 设定一个变量，如果为true，则执行exe函数
     *
     * @param isRun 变量
     */
    public IsRunFunction(boolean isRun) {
        this.isRun = isRun;
    }

    /**
     * 当 isRun == true 时执行此函数
     *
     * @param function 函数
     * @return 对象自身
     */
    public IsRunFunction exe(AuthFunction function) {
        if (isRun) {
            function.run();
        }
        return this;
    }

    /**
     * 当 isRun == false 时执行此函数
     *
     * @param function 函数
     * @return 对象自身
     */
    public IsRunFunction noExe(AuthFunction function) {
        if (!isRun) {
            function.run();
        }
        return this;
    }

}
