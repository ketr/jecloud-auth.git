/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.context;

import com.je.auth.check.AuthCheckEngine;
import com.je.auth.check.context.model.AuthRequest;
import com.je.auth.check.context.model.AuthResponse;
import com.je.auth.check.context.model.AuthStorage;

/**
 * Sa-Token 上下文处理器
 *
 * @author kong
 */
public interface AuthTokenContext {

	AuthCheckEngine getAuthCheckEngine();

	void setAuthCheckEngine(AuthCheckEngine authCheckEngine);

	/**
	 * 获取当前请求的 [Request] 对象
	 *
	 * @return see note 
	 */
	AuthRequest getRequest();

	/**
	 * 获取当前请求的 [Response] 对象
	 * 
	 * @return see note 
	 */
	AuthResponse getResponse();

	/**
	 * 获取当前请求的 [存储器] 对象 
	 * 
	 * @return see note 
	 */
	AuthStorage getStorage();

	/**
	 * 校验指定路由匹配符是否可以匹配成功指定路径 
	 * 
	 * @param pattern 路由匹配符 
	 * @param path 需要匹配的路径 
	 * @return see note 
	 */
	boolean matchPath(String pattern, String path);

	/**
	 * 此上下文是否有效 
	 * @return / 
	 */
	default boolean isValid() {
		return false;
	}
	
}
