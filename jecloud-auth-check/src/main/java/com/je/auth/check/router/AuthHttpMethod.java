/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.router;

import com.je.auth.check.exception.TokenException;

import java.util.HashMap;
import java.util.Map;

/**
 * Http 请求各种请求类型的枚举表示
 *
 * <p> 参考：Spring - HttpMethod
 *
 * @author kong
 */
public enum AuthHttpMethod {

    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE, CONNECT,

    /**
     * 代表全部请求方式
     */
    ALL;

    private static final Map<String, AuthHttpMethod> map = new HashMap<>();

    static {
        for (AuthHttpMethod reqMethod : values()) {
            map.put(reqMethod.name(), reqMethod);
        }
    }

    /**
     * String 转 enum
     *
     * @param method 请求类型
     * @return ReqMethod 对象
     */
    public static AuthHttpMethod toEnum(String method) {
        if (method == null) {
            throw new TokenException("无效Method：" + method);
        }
        AuthHttpMethod reqMethod = map.get(method.toUpperCase());
        if (reqMethod == null) {
            throw new TokenException("无效Method：" + method);
        }
        return reqMethod;
    }

    /**
     * String[] 转 enum[]
     *
     * @param methods 请求类型数组
     * @return ReqMethod 对象
     */
    public static AuthHttpMethod[] toEnumArray(String... methods) {
        AuthHttpMethod[] arr = new AuthHttpMethod[methods.length];
        for (int i = 0; i < methods.length; i++) {
            arr[i] = AuthHttpMethod.toEnum(methods[i]);
        }
        return arr;
    }

}
