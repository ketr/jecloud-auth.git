/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.context.model;

/**
 * [存储器] 包装类
 * <p> 在 Request作用域里: 存值、取值
 * @author kong
 *
 */
public interface AuthStorage {

	/**
	 * 获取底层源对象 
	 * @return see note 
	 */
	Object getSource();

	/**
	 * 在 [Request作用域] 里写入一个值 
	 * @param key 键 
	 * @param value 值
	 */
	void set(String key, Object value);
	
	/**
	 * 在 [Request作用域] 里获取一个值 
	 * @param key 键 
	 * @return 值 
	 */
	Object get(String key);

	/**
	 * 在 [Request作用域] 里删除一个值 
	 * @param key 键 
	 */
	void delete(String key);

}
