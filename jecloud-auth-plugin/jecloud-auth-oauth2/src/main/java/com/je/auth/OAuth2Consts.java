/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

/**
 * OAuth2 所有常量
 *
 * @author kong
 */
public class OAuth2Consts {

    /**
     * 所有API接口
     *
     * @author kong
     */
    public static final class Api {
        public static String authorize = "/oauth2/authorize";
        public static String token = "/oauth2/token";
        public static String refresh = "/oauth2/refresh";
        public static String revoke = "/oauth2/revoke";
        public static String client_token = "/oauth2/client_token";
        public static String doLogin = "/oauth2/doLogin";
        public static String doConfirm = "/oauth2/doConfirm";
        public static String doLoadResource = "/oauth2/doLoadResource";
    }

    /**
     * 所有参数名称
     *
     * @author kong
     */
    public static final class Param {
        public static String response_type = "response_type";
        public static String client_id = "client_id";
        public static String client_secret = "client_secret";
        public static String redirect_uri = "redirect_uri";
        public static String scope = "scope";
        public static String state = "state";
        public static String code = "code";
        public static String token = "token";
        public static String access_token = "access_token";
        public static String refresh_token = "refresh_token";
        public static String grant_type = "grant_type";
        public static String username = "username";
        public static String password = "password";
        public static String name = "name";
        public static String pwd = "pwd";
    }

    /**
     * 所有返回类型
     */
    public static final class ResponseType {

        /**
         * 返回授权码
         */
        public static String code = "code";

        /**
         * 返回token
         */
        public static String token = "token";
    }

    /**
     * 所有授权类型
     */
    public static final class GrantType {

        /**
         * 授权码类型
         */
        public static String authorization_code = "authorization_code";

        /**
         * 密码试类型
         */
        public static String password = "password";

        /**
         * 客户端凭证授权类型
         */
        public static String client_credentials = "client_credentials";

        /**
         * 隐藏式
         */
        public static String implicit = "implicit";

        /**
         * 刷新token
         */
        public static String refresh_token = "refresh_token";
    }

    /**
     * 表示OK的返回结果
     */
    public static final String OK = "ok";

    /**
     * 表示请求没有得到任何有效处理 {msg: "not handle"}
     */
    public static final String NOT_HANDLE = "{\"msg\": \"not handle\"}";

}
