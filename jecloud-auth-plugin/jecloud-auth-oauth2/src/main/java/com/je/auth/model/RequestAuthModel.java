/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.model;

import com.je.auth.exception.OAuth2Exception;
import com.je.auth.check.util.FoxUtil;

import java.io.Serializable;

/**
 * 请求授权参数的Model
 *
 * @author kong
 */
public class RequestAuthModel implements Serializable {

    private static final long serialVersionUID = -6541180061782004705L;

    /**
     * 应用id
     */
    public String clientId;

    /**
     * 授权范围
     */
    public String scope;

    /**
     * 对应的账号id
     */
    public Object loginId;

    /**
     * 待重定向URL
     */
    public String redirectUri;

    /**
     * 授权类型, 非必填
     */
    public String responseType;

    /**
     * 状态标识, 可为null
     */
    public String state;

    /**
     * @return clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId 要设置的 clientId
     * @return 对象自身
     */
    public RequestAuthModel setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    /**
     * @return scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * @param scope 要设置的 scope
     * @return 对象自身
     */
    public RequestAuthModel setScope(String scope) {
        this.scope = scope;
        return this;
    }

    /**
     * @return loginId
     */
    public Object getLoginId() {
        return loginId;
    }

    /**
     * @param loginId 要设置的 loginId
     * @return 对象自身
     */
    public RequestAuthModel setLoginId(Object loginId) {
        this.loginId = loginId;
        return this;
    }

    /**
     * @return redirectUri
     */
    public String getRedirectUri() {
        return redirectUri;
    }

    /**
     * @param redirectUri 要设置的 redirectUri
     * @return 对象自身
     */
    public RequestAuthModel setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
        return this;
    }

    /**
     * @return responseType
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * @param responseType 要设置的 responseType
     * @return 对象自身
     */
    public RequestAuthModel setResponseType(String responseType) {
        this.responseType = responseType;
        return this;
    }

    /**
     * @return state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state 要设置的 state
     * @return 对象自身
     */
    public RequestAuthModel setState(String state) {
        this.state = state;
        return this;
    }

    /**
     * 检查此Model参数是否有效
     *
     * @return 对象自身
     */
    public RequestAuthModel checkModel() throws OAuth2Exception {
        if (FoxUtil.isEmpty(clientId)) {
            throw new OAuth2Exception("无效client_id");
        }
        if (FoxUtil.isEmpty(scope)) {
            throw new OAuth2Exception("无效scope");
        }
        if (FoxUtil.isEmpty(redirectUri)) {
            throw new OAuth2Exception("无效redirect_uri");
        }
        if (FoxUtil.isEmpty(String.valueOf(loginId))) {
            throw new OAuth2Exception("无效LoginId");
        }
        return this;
    }


}
