/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.config;

import java.io.Serializable;

/**
 * OAuth2 配置类 Model
 */
public class OAuth2Config implements Serializable {

    private static final long serialVersionUID = -6541180061782004705L;

    /**
     * 是否打开模式：授权码（Authorization Code）
     */
    private Boolean isCode = true;

    /**
     * 是否打开模式：隐藏式（Implicit）
     */
    private Boolean isImplicit = false;

    /**
     * 是否打开模式：密码式（Password）
     */
    private Boolean isPassword = false;

    /**
     * 是否打开模式：凭证式（Client Credentials）
     */
    private Boolean isClient = false;

    /**
     * 是否在每次 Refresh-Token 刷新 Access-Token 时，产生一个新的 Refresh-Token
     */
    private Boolean isNewRefresh = false;

    /**
     * Code授权码 保存的时间(单位：秒) 默认五分钟
     */
    private long codeTimeout = 60 * 5;

    /**
     * Access-Token 保存的时间(单位：秒) 默认两个小时
     */
    private long accessTokenTimeout = 60 * 60 * 2;

    /**
     * Refresh-Token 保存的时间(单位：秒) 默认30 天
     */
    private long refreshTokenTimeout = 60 * 60 * 24 * 30;

    /**
     * Client-Token 保存的时间(单位：秒) 默认两个小时
     */
    private long clientTokenTimeout = 60 * 60 * 2;

    /**
     * Past-Client-Token 保存的时间(单位：秒) 默认为 -1，代表延续 Client-Token有效期
     */
    private long pastClientTokenTimeout = -1;
    /**
     * 登录地址，用于用户未登录，跳转到的登录页
     */
    private String loginView;
    /**
     * 认证地址，用于用户确认授权页面
     */
    private String confirmView;

    /**
     * @return isCode
     */
    public Boolean getIsCode() {
        return isCode;
    }

    /**
     * @param isCode 要设置的 isCode
     */
    public void setIsCode(Boolean isCode) {
        this.isCode = isCode;
    }

    /**
     * @return isImplicit
     */
    public Boolean getIsImplicit() {
        return isImplicit;
    }

    /**
     * @param isImplicit 要设置的 isImplicit
     */
    public void setIsImplicit(Boolean isImplicit) {
        this.isImplicit = isImplicit;
    }

    /**
     * @return isPassword
     */
    public Boolean getIsPassword() {
        return isPassword;
    }

    /**
     * @param isPassword 要设置的 isPassword
     */
    public void setIsPassword(Boolean isPassword) {
        this.isPassword = isPassword;
    }

    /**
     * @return isClient
     */
    public Boolean getIsClient() {
        return isClient;
    }

    /**
     * @param isClient 要设置的 isClient
     */
    public void setIsClient(Boolean isClient) {
        this.isClient = isClient;
    }

    /**
     * @return isNewRefresh
     */
    public Boolean getIsNewRefresh() {
        return isNewRefresh;
    }

    /**
     * @param isNewRefresh 要设置的 isNewRefresh
     */
    public void setIsNewRefresh(Boolean isNewRefresh) {
        this.isNewRefresh = isNewRefresh;
    }

    /**
     * @return codeTimeout
     */
    public long getCodeTimeout() {
        return codeTimeout;
    }

    /**
     * @param codeTimeout 要设置的 codeTimeout
     * @return 对象自身
     */
    public OAuth2Config setCodeTimeout(long codeTimeout) {
        this.codeTimeout = codeTimeout;
        return this;
    }

    /**
     * @return accessTokenTimeout
     */
    public long getAccessTokenTimeout() {
        return accessTokenTimeout;
    }

    /**
     * @param accessTokenTimeout 要设置的 accessTokenTimeout
     * @return 对象自身
     */
    public OAuth2Config setAccessTokenTimeout(long accessTokenTimeout) {
        this.accessTokenTimeout = accessTokenTimeout;
        return this;
    }

    /**
     * @return refreshTokenTimeout
     */
    public long getRefreshTokenTimeout() {
        return refreshTokenTimeout;
    }

    /**
     * @param refreshTokenTimeout 要设置的 refreshTokenTimeout
     * @return 对象自身
     */
    public OAuth2Config setRefreshTokenTimeout(long refreshTokenTimeout) {
        this.refreshTokenTimeout = refreshTokenTimeout;
        return this;
    }

    /**
     * @return clientTokenTimeout
     */
    public long getClientTokenTimeout() {
        return clientTokenTimeout;
    }

    /**
     * @param clientTokenTimeout 要设置的 clientTokenTimeout
     * @return 对象自身
     */
    public OAuth2Config setClientTokenTimeout(long clientTokenTimeout) {
        this.clientTokenTimeout = clientTokenTimeout;
        return this;
    }

    /**
     * @return pastClientTokenTimeout
     */
    public long getPastClientTokenTimeout() {
        return pastClientTokenTimeout;
    }

    /**
     * @param pastClientTokenTimeout 要设置的 pastClientTokenTimeout
     * @return 对象自身
     */
    public OAuth2Config setPastClientTokenTimeout(long pastClientTokenTimeout) {
        this.pastClientTokenTimeout = pastClientTokenTimeout;
        return this;
    }

    public String getLoginView() {
        return loginView;
    }

    public OAuth2Config setLoginView(String loginView) {
        this.loginView = loginView;
        return this;
    }

    public String getConfirmView() {
        return confirmView;
    }

    public OAuth2Config setConfirmView(String confirmView) {
        this.confirmView = confirmView;
        return this;
    }

    @Override
    public String toString() {
        return "OAuth2Config{" +
                "isCode=" + isCode +
                ", isImplicit=" + isImplicit +
                ", isPassword=" + isPassword +
                ", isClient=" + isClient +
                ", isNewRefresh=" + isNewRefresh +
                ", codeTimeout=" + codeTimeout +
                ", accessTokenTimeout=" + accessTokenTimeout +
                ", refreshTokenTimeout=" + refreshTokenTimeout +
                ", clientTokenTimeout=" + clientTokenTimeout +
                ", pastClientTokenTimeout=" + pastClientTokenTimeout +
                ", loginView='" + loginView + '\'' +
                ", confirmView='" + confirmView + '\'' +
                '}';
    }

}
