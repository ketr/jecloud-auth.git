/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.config.OAuth2Config;

/**
 * OAuth2引擎
 */
public class OAuth2Engine {

    private AuthEngine authEngine;

    private OAuth2Config oAuth2Config;

    private OAuth2Template auth2Template;

    private OAuth2Handle oAuth2Handle;

    public OAuth2Engine(AuthEngine authEngine,
                        OAuth2Config oAuth2Config, OAuth2Template auth2Template,
                        OAuth2Handle oAuth2Handle) {
        this.authEngine = authEngine;
        this.oAuth2Config = oAuth2Config;
        this.auth2Template = auth2Template;
        this.auth2Template.setoAuth2Engine(this);
        this.oAuth2Handle = oAuth2Handle;
        this.oAuth2Handle.setAuthEngine(authEngine);
        this.oAuth2Handle.setOAuth2Engine(this);
    }

    public AuthEngine getAuthEngine() {
        return authEngine;
    }

    public OAuth2Config getoAuth2Config() {
        return oAuth2Config;
    }

    public OAuth2Template getAuth2Template() {
        return auth2Template;
    }

    public OAuth2Handle getoAuth2Handle() {
        return oAuth2Handle;
    }


}
