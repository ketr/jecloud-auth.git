/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.model;

import java.io.Serializable;

/**
 * Client应用信息 Model
 *
 * @author kong
 */
public class ClientModel implements Serializable {

    private static final long serialVersionUID = -6541180061782004705L;

    /**
     * 应用id
     */
    public String clientId;
    /**
     * 应用秘钥
     */
    public String clientSecret;
    /**
     * 应用签约的所有权限, 多个用逗号隔开
     */
    public String contractScope;
    /**
     * 应用允许授权的所有URL, 多个用逗号隔开
     */
    public String allowUrl;
    /**
     * 此 Client 是否打开模式：授权码（Authorization Code）
     */
    public Boolean isCode = false;
    /**
     * 此 Client 是否打开模式：隐藏式（Implicit）
     */
    public Boolean isImplicit = false;
    /**
     * 此 Client 是否打开模式：密码式（Password）
     */
    public Boolean isPassword = false;
    /**
     * 此 Client 是否打开模式：凭证式（Client Credentials）
     */
    public Boolean isClient = false;
    /**
     * 是否自动判断此 Client 开放的授权模式
     * <br> 此值为true时：四种模式（isCode、isImplicit、isPassword、isClient）是否生效，依靠全局设置
     * <br> 此值为false时：四种模式（isCode、isImplicit、isPassword、isClient）是否生效，依靠局部配置+全局配置
     */
    public Boolean isAutoMode = true;
    /**
     * 单独配置此Client：是否在每次 Refresh-Token 刷新 Access-Token 时，产生一个新的 Refresh-Token [默认取全局配置]
     */
    public Boolean isNewRefresh = false;
    /**
     * 单独配置此Client：Access-Token 保存的时间(单位秒)  [默认取全局配置]
     */
    public long accessTokenTimeout = 60;
    /**
     * 单独配置此Client：Refresh-Token 保存的时间(单位秒) [默认取全局配置]
     */
    public long refreshTokenTimeout;
    /**
     * 单独配置此Client：Client-Token 保存的时间(单位秒) [默认取全局配置]
     */
    public long clientTokenTimeout = 60;
    /**
     * 单独配置此Client：Past-Client-Token 保存的时间(单位：秒) [默认取全局配置]
     */
    public long pastClientTokenTimeout = 60;

    public ClientModel() {
    }

    public ClientModel(String clientId, String clientSecret, String contractScope, String allowUrl) {
        super();
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.contractScope = contractScope;
        this.allowUrl = allowUrl;
    }

    /**
     * @return 应用id
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId 应用id
     * @return 对象自身
     */
    public ClientModel setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    /**
     * @return 应用秘钥
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * @param clientSecret 应用秘钥
     * @return 对象自身
     */
    public ClientModel setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
        return this;
    }

    /**
     * @return 应用签约的所有权限, 多个用逗号隔开
     */
    public String getContractScope() {
        return contractScope;
    }

    /**
     * @param contractScope 应用签约的所有权限, 多个用逗号隔开
     * @return 对象自身
     */
    public ClientModel setContractScope(String contractScope) {
        this.contractScope = contractScope;
        return this;
    }

    /**
     * @return 应用允许授权的所有URL, 多个用逗号隔开
     */
    public String getAllowUrl() {
        return allowUrl;
    }

    /**
     * @param allowUrl 应用允许授权的所有URL, 多个用逗号隔开
     * @return 对象自身
     */
    public ClientModel setAllowUrl(String allowUrl) {
        this.allowUrl = allowUrl;
        return this;
    }

    /**
     * @return 此 Client 是否打开模式：授权码（Authorization Code）
     */
    public Boolean getIsCode() {
        return isCode;
    }

    /**
     * @param isCode 此 Client 是否打开模式：授权码（Authorization Code）
     * @return 对象自身
     */
    public ClientModel setIsCode(Boolean isCode) {
        this.isCode = isCode;
        return this;
    }

    /**
     * @return 此 Client 是否打开模式：隐藏式（Implicit）
     */
    public Boolean getIsImplicit() {
        return isImplicit;
    }

    /**
     * @param isImplicit 此 Client 是否打开模式：隐藏式（Implicit）
     * @return 对象自身
     */
    public ClientModel setIsImplicit(Boolean isImplicit) {
        this.isImplicit = isImplicit;
        return this;
    }

    /**
     * @return 此 Client 是否打开模式：密码式（Password）
     */
    public Boolean getIsPassword() {
        return isPassword;
    }

    /**
     * @param isPassword 此 Client 是否打开模式：密码式（Password）
     * @return 对象自身
     */
    public ClientModel setIsPassword(Boolean isPassword) {
        this.isPassword = isPassword;
        return this;
    }

    /**
     * @return 此 Client 是否打开模式：凭证式（Client Credentials）
     * @return 对象自身
     */
    public Boolean getIsClient() {
        return isClient;
    }

    /**
     * @param isClient 此 Client 是否打开模式：凭证式（Client Credentials）
     * @return 对象自身
     */
    public ClientModel setIsClient(Boolean isClient) {
        this.isClient = isClient;
        return this;
    }

    /**
     * @return 是否自动判断此 Client 开放的授权模式
     */
    public Boolean getIsAutoMode() {
        return isAutoMode;
    }

    /**
     * @param isAutoMode 是否自动判断此 Client 开放的授权模式
     * @return 对象自身
     */
    public ClientModel setIsAutoMode(Boolean isAutoMode) {
        this.isAutoMode = isAutoMode;
        return this;
    }


    /**
     * @return 此Client：是否在每次 Refresh-Token 刷新 Access-Token 时，产生一个新的 Refresh-Token [默认取全局配置]
     */
    public Boolean getIsNewRefresh() {
        return isNewRefresh;
    }

    /**
     * @param isNewRefresh 单独配置此Client：是否在每次 Refresh-Token 刷新 Access-Token 时，产生一个新的 Refresh-Token [默认取全局配置]
     * @return 对象自身
     */
    public ClientModel setIsNewRefresh(Boolean isNewRefresh) {
        this.isNewRefresh = isNewRefresh;
        return this;
    }

    /**
     * @return 此Client：Access-Token 保存的时间(单位秒)  [默认取全局配置]
     */
    public long getAccessTokenTimeout() {
        return accessTokenTimeout;
    }

    /**
     * @param accessTokenTimeout 单独配置此Client：Access-Token 保存的时间(单位秒)  [默认取全局配置]
     * @return 对象自身
     */
    public ClientModel setAccessTokenTimeout(long accessTokenTimeout) {
        this.accessTokenTimeout = accessTokenTimeout;
        return this;
    }

    /**
     * @return 此Client：Refresh-Token 保存的时间(单位秒) [默认取全局配置]
     */
    public long getRefreshTokenTimeout() {
        return refreshTokenTimeout;
    }

    /**
     * @param refreshTokenTimeout 单独配置此Client：Refresh-Token 保存的时间(单位秒) [默认取全局配置]
     * @return 对象自身
     */
    public ClientModel setRefreshTokenTimeout(long refreshTokenTimeout) {
        this.refreshTokenTimeout = refreshTokenTimeout;
        return this;
    }

    /**
     * @return 此Client：Client-Token 保存的时间(单位秒) [默认取全局配置]
     */
    public long getClientTokenTimeout() {
        return clientTokenTimeout;
    }

    /**
     * @param clientTokenTimeout 单独配置此Client：Client-Token 保存的时间(单位秒) [默认取全局配置]
     * @return 对象自身
     */
    public ClientModel setClientTokenTimeout(long clientTokenTimeout) {
        this.clientTokenTimeout = clientTokenTimeout;
        return this;
    }

    /**
     * @return 此Client：Past-Client-Token 保存的时间(单位：秒) [默认取全局配置]
     */
    public long getPastClientTokenTimeout() {
        return pastClientTokenTimeout;
    }

    /**
     * @param pastClientTokenTimeout 单独配置此Client：Past-Client-Token 保存的时间(单位：秒) [默认取全局配置]
     * @return 对象自身
     */
    public ClientModel setPastClientTokenTimeout(long pastClientTokenTimeout) {
        this.pastClientTokenTimeout = pastClientTokenTimeout;
        return this;
    }

    @Override
    public String toString() {
        return "ClientModel{" +
                "clientId='" + clientId + '\'' +
                ", clientSecret='" + clientSecret + '\'' +
                ", contractScope='" + contractScope + '\'' +
                ", allowUrl='" + allowUrl + '\'' +
                ", isCode=" + isCode +
                ", isImplicit=" + isImplicit +
                ", isPassword=" + isPassword +
                ", isClient=" + isClient +
                ", isAutoMode=" + isAutoMode +
                ", isNewRefresh=" + isNewRefresh +
                ", accessTokenTimeout=" + accessTokenTimeout +
                ", refreshTokenTimeout=" + refreshTokenTimeout +
                ", clientTokenTimeout=" + clientTokenTimeout +
                ", pastClientTokenTimeout=" + pastClientTokenTimeout +
                '}';
    }
}
