/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.model;

import com.je.auth.AuthEngine;
import com.je.auth.check.AuthCheckEngine;
import com.je.auth.check.context.model.AuthRequest;
import com.je.auth.check.exception.TokenException;
import com.je.auth.check.util.FoxUtil;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Request for Servlet
 */
public class AuthRequestForServlet implements AuthRequest {

	private AuthCheckEngine authCheckEngine;
	/**
	 * 底层Request对象
	 */
	protected HttpServletRequest request;

	public AuthRequestForServlet(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public AuthCheckEngine getAuthCheckEngine() {
		return authCheckEngine;
	}

	@Override
	public void setAuthCheckEngine(AuthCheckEngine authCheckEngine) {
		this.authCheckEngine = authCheckEngine;
	}

	/**
	 * 获取底层源对象
	 */
	@Override
	public Object getSource() {
		return request;
	}

	/**
	 * 在 [请求体] 里获取一个值
	 */
	@Override
	public String getParam(String name) {
		return request.getParameter(name);
	}

	/**
	 * 在 [请求头] 里获取一个值
	 */
	@Override
	public String getHeader(String name) {
		return request.getHeader(name);
	}

	/**
	 * 在 [Cookie作用域] 里获取一个值
	 */
	@Override
	public String getCookieValue(String name) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie != null && name.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * 返回当前请求path (不包括上下文名称)
	 */
	@Override
	public String getRequestPath() {
		return request.getServletPath();
	}

	/**
	 * 返回当前请求的url，例：http://xxx.com/test
	 *
	 * @return see note
	 */
	@Override
	public String getUrl() {
		String currDomain = getAuthCheckEngine().getConfig().getCurrDomain();
		if (FoxUtil.isEmpty(currDomain) == false) {
			return currDomain + this.getRequestPath();
		}
		return request.getRequestURL().toString();
	}

	/**
	 * 返回当前请求的类型
	 */
	@Override
	public String getMethod() {
		return request.getMethod();
	}

	/**
	 * 转发请求
	 */
	@Override
	public Object forward(String path) {
		try {
			HttpServletResponse response = (HttpServletResponse) getAuthCheckEngine().getAuthTokenContext().getResponse().getSource();
			request.getRequestDispatcher(path).forward(request, response);
			return null;
		} catch (ServletException | IOException e) {
			throw new TokenException(e);
		}
	}

}
