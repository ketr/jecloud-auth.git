/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.model;

import com.je.auth.check.context.model.AuthStorage;

import javax.servlet.http.HttpServletRequest;

/**
 * Storage for Servlet
 *
 * @author kong
 */
public class AuthStorageForServlet implements AuthStorage {

	/**
	 * 底层Request对象
	 */
	protected HttpServletRequest request;

	/**
	 * 实例化
	 *
	 * @param request request对象
	 */
	public AuthStorageForServlet(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * 获取底层源对象
	 */
	@Override
	public Object getSource() {
		return request;
	}

	/**
	 * 在 [Request作用域] 里写入一个值
	 */
	@Override
	public void set(String key, Object value) {
		request.setAttribute(key, value);
	}

	/**
	 * 在 [Request作用域] 里获取一个值
	 */
	@Override
	public Object get(String key) {
		return request.getAttribute(key);
	}

	/**
	 * 在 [Request作用域] 里删除一个值
	 */
	@Override
	public void delete(String key) {
		request.removeAttribute(key);
	}

}
