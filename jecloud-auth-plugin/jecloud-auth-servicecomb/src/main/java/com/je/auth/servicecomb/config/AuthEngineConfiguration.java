/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.servicecomb.config;

import com.je.auth.*;
import com.je.auth.check.AuthCheckEngine;
import com.je.auth.config.TokenConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthEngineConfiguration {

    @Bean
    public AuthEngine authEngine(TokenConfig config,
                                 AuthCheckEngine authCheckEngine,
                                 AuthLoginManager authLoginManager,
                                 AuthTokenDao authTokenDao,
                                 AuthTempInterface authTempInterface,
                                 AuthSessionTemplate authSessionTemplate, AuthSessionCustomTemplate authSessionCustomTemplate) {
        return new AuthEngine(config, authCheckEngine, authLoginManager,
                authTokenDao, authTempInterface, authSessionTemplate,
                authSessionCustomTemplate);
    }

    @Bean
    @ConfigurationProperties(prefix = "jecloud.auth.login")
    public TokenConfig tokenConfig() {
        return new TokenConfig();
    }

    @Bean
    public AuthSessionTemplate authSessionTemplate() {
        return new AuthSessionTemplate();
    }

    @Bean
    public AuthSessionCustomTemplate authSessionCustomTemplate() {
        return new AuthSessionCustomTemplate();
    }

}
