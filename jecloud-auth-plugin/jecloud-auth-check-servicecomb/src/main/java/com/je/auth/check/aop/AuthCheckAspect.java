/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.aop;

import com.je.auth.check.AuthCheckEngine;
import com.je.auth.check.config.CheckConfig;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 基于 Spring Aop 的注解鉴权
 *
 * @author kong
 */
@Aspect
@Component
@Order(CheckConfig.ASSEMBLY_ORDER)
public class AuthCheckAspect {

    private static final Logger logger = LoggerFactory.getLogger(AuthCheckAspect.class);

    @Autowired
    private AuthCheckEngine authCheckEngine;

    public AuthCheckAspect() {

    }

    /**
     * 定义AOP签名 (切入所有使用鉴权注解的方法)
     */
    public static final String POINTCUT_SIGN =
            "@within(com.je.auth.check.annotation.AuthCheckLogin) || @annotation(com.je.auth.check.annotation.AuthCheckLogin) || "
                    + "@within(com.je.auth.check.annotation.AuthCheckRole) || @annotation(com.je.auth.check.annotation.AuthCheckRole) || "
                    + "@within(com.je.auth.check.annotation.AuthCheckPermission) || @annotation(com.je.auth.check.annotation.AuthCheckPermission) || "
                    + "@within(com.je.auth.check.annotation.AuthCheckDept) || @annotation(com.je.auth.check.annotation.AuthCheckDept) || "
                    + "@within(com.je.auth.check.annotation.AuthCheckDataPermission) || @annotation(com.je.auth.check.annotation.AuthCheckDataPermission) || "
                    + "@within(com.je.auth.check.annotation.AuthCheckOrg) || @annotation(com.je.auth.check.annotation.AuthCheckOrg)";

    /**
     * 声明AOP签名
     */
    @Pointcut(POINTCUT_SIGN)
    public void pointcut() {
    }

    /**
     * 环绕切入
     *
     * @param joinPoint 切面对象
     * @return 底层方法执行后的返回值
     * @throws Throwable 底层方法抛出的异常
     */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 注解鉴权
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        logger.info("开始对方法" + signature.getMethod().getName() + "进行鉴权。");
        long time = System.currentTimeMillis();
        authCheckEngine.getAuthCheckStrategy().checkMethodAnnotation.accept(signature.getMethod());
        logger.info(signature.getMethod().getName() + "方法鉴权花费时间为：" + (System.currentTimeMillis() - time));
        try {
            // 执行原有逻辑
            Object obj = joinPoint.proceed();
            return obj;
        } catch (Throwable e) {
            throw e;
        }
    }

}
