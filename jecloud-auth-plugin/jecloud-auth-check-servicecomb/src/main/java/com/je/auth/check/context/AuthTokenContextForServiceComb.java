/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.check.context;

import com.je.auth.check.AuthCheckEngine;
import com.je.auth.check.context.model.AuthRequest;
import com.je.auth.check.context.model.AuthResponse;
import com.je.auth.check.context.model.AuthStorage;
import com.je.auth.check.exception.ApiDisabledException;
import com.je.auth.check.model.AuthRequestForServiceComb;
import com.je.auth.check.model.AuthResponseForServiceComb;
import com.je.auth.check.model.AuthStorageForServiceComb;
import org.apache.servicecomb.swagger.invocation.context.ContextUtils;

public class AuthTokenContextForServiceComb implements AuthTokenContext {

    private AuthCheckEngine authCheckEngine;

    @Override
    public AuthCheckEngine getAuthCheckEngine() {
        return authCheckEngine;
    }

    @Override
    public void setAuthCheckEngine(AuthCheckEngine authCheckEngine) {
        this.authCheckEngine = authCheckEngine;
    }

    @Override
    public AuthRequest getRequest() {
        return new AuthRequestForServiceComb(ContextUtils.getInvocationContext());
    }

    @Override
    public AuthResponse getResponse() {
        return new AuthResponseForServiceComb(ContextUtils.getInvocationContext());
    }

    @Override
    public AuthStorage getStorage() {
        return new AuthStorageForServiceComb(ContextUtils.getInvocationContext());
    }

    @Override
    public boolean matchPath(String pattern, String path) {
        throw new ApiDisabledException();
    }

}
