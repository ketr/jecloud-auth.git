/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.config;

import java.io.Serializable;

/**
 * Token 配置类 Model
 * <p>
 * 你可以通过yml、properties、java代码等形式配置本类参数，具体请查阅官方文档: http://sa-token.dev33.cn/
 *
 * @author kong
 */
public class TokenConfig implements Serializable {

    private static final long serialVersionUID = -6541180061782004705L;

    /**
     * token名称 (同时也是cookie名称)
     */
    private String tokenName = "authorization";

    /**
     * token的长久有效期(单位:秒) 默认30天, -1代表永久
     */
    private long timeout = 60 * 60 * 24 * 30;

    /**
     * token临时有效期 [指定时间内无操作就视为token过期] (单位: 秒), 默认-1 代表不限制
     * (例如可以设置为1800代表30分钟内无操作就过期)
     */
    private long activityTimeout = -1;

    /**
     * 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
     */
    private Boolean isConcurrent = true;

    /**
     * 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
     */
    private Boolean isShare = true;

    /**
     * token风格(默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik)
     */
    private String tokenStyle = "uuid";

    /**
     * 默认dao层实现类中，每次清理过期数据间隔的时间 (单位: 秒) ，默认值30秒，设置为-1代表不启动定时清理
     */
    private int dataRefreshPeriod = 30;

    /**
     * 获取[token专属session]时是否必须登录 (如果配置为true，会在每次获取[token-session]时校验是否登录)
     */
    private Boolean tokenSessionCheckLogin = true;

    /**
     * 是否打开自动续签 (如果此值为true, 框架会在每次直接或间接调用getLoginId()时进行一次过期检查与续签操作)
     */
    private Boolean autoRenew = true;

    /**
     * token前缀, 格式样例(satoken: Bearer xxxx-xxxx-xxxx-xxxx)
     */
    private String tokenPrefix;

    /**
     * 是否打印操作日志
     */
    private Boolean isLog = false;

    /**
     * jwt秘钥 (只有集成 jwt 模块时此参数才会生效)
     */
    private String jwtSecretKey;

    /**
     * Id-Token的有效期 (单位: 秒)
     */
    private long idTokenTimeout = 60 * 60 * 24;

    /**
     * 是否校验Id-Token（部分rpc插件有效）
     */
    private Boolean checkIdToken = false;

    /**
     * Cookie配置对象
     */
    public CookieConfig cookie = new CookieConfig();

    /**
     * @return token名称 (同时也是cookie名称)
     */
    public String getTokenName() {
        return tokenName;
    }

    /**
     * @param tokenName token名称 (同时也是cookie名称)
     * @return 对象自身
     */
    public TokenConfig setTokenName(String tokenName) {
        this.tokenName = tokenName;
        return this;
    }

    /**
     * @return token的长久有效期(单位 : 秒) 默认30天, -1代表永久
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * @param timeout token的长久有效期(单位:秒) 默认30天, -1代表永久
     * @return 对象自身
     */
    public TokenConfig setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    /**
     * @return token临时有效期 [指定时间内无操作就视为token过期] (单位: 秒), 默认-1 代表不限制
     * (例如可以设置为1800代表30分钟内无操作就过期)
     */
    public long getActivityTimeout() {
        return activityTimeout;
    }

    /**
     * @param activityTimeout token临时有效期 [指定时间内无操作就视为token过期] (单位: 秒), 默认-1 代表不限制
     *                        (例如可以设置为1800代表30分钟内无操作就过期)
     * @return 对象自身
     */
    public TokenConfig setActivityTimeout(long activityTimeout) {
        this.activityTimeout = activityTimeout;
        return this;
    }

    /**
     * @return 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
     */
    public Boolean getIsConcurrent() {
        return isConcurrent;
    }

    /**
     * @param isConcurrent 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
     * @return 对象自身
     */
    public TokenConfig setIsConcurrent(Boolean isConcurrent) {
        this.isConcurrent = isConcurrent;
        return this;
    }

    /**
     * @return 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
     */
    public Boolean getIsShare() {
        return isShare;
    }

    /**
     * @param isShare 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
     * @return 对象自身
     */
    public TokenConfig setIsShare(Boolean isShare) {
        this.isShare = isShare;
        return this;
    }

    /**
     * @return token风格(默认可取值 ： uuid 、 simple - uuid 、 random - 32 、 random - 64 、 random - 128 、 tik)
     */
    public String getTokenStyle() {
        return tokenStyle;
    }

    /**
     * @param tokenStyle token风格(默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik)
     * @return 对象自身
     */
    public TokenConfig setTokenStyle(String tokenStyle) {
        this.tokenStyle = tokenStyle;
        return this;
    }

    /**
     * @return 默认dao层实现类中，每次清理过期数据间隔的时间 (单位: 秒) ，默认值30秒，设置为-1代表不启动定时清理
     */
    public int getDataRefreshPeriod() {
        return dataRefreshPeriod;
    }

    /**
     * @param dataRefreshPeriod 默认dao层实现类中，每次清理过期数据间隔的时间 (单位: 秒)
     *                          ，默认值30秒，设置为-1代表不启动定时清理
     * @return 对象自身
     */
    public TokenConfig setDataRefreshPeriod(int dataRefreshPeriod) {
        this.dataRefreshPeriod = dataRefreshPeriod;
        return this;
    }

    /**
     * @return 获取[token专属session]时是否必须登录 (如果配置为true，会在每次获取[token-session]时校验是否登录)
     */
    public Boolean getTokenSessionCheckLogin() {
        return tokenSessionCheckLogin;
    }

    /**
     * @param tokenSessionCheckLogin 获取[token专属session]时是否必须登录
     *                               (如果配置为true，会在每次获取[token-session]时校验是否登录)
     * @return 对象自身
     */
    public TokenConfig setTokenSessionCheckLogin(Boolean tokenSessionCheckLogin) {
        this.tokenSessionCheckLogin = tokenSessionCheckLogin;
        return this;
    }

    /**
     * @return 是否打开了自动续签 (如果此值为true, 框架会在每次直接或间接调用getLoginId()时进行一次过期检查与续签操作)
     */
    public Boolean getAutoRenew() {
        return autoRenew;
    }

    /**
     * @param autoRenew 是否打开自动续签 (如果此值为true, 框架会在每次直接或间接调用getLoginId()时进行一次过期检查与续签操作)
     * @return 对象自身
     */
    public TokenConfig setAutoRenew(Boolean autoRenew) {
        this.autoRenew = autoRenew;
        return this;
    }

    /**
     * @return token前缀, 格式样例(satoken: Bearer xxxx-xxxx-xxxx-xxxx)
     */
    public String getTokenPrefix() {
        return tokenPrefix;
    }

    /**
     * @param tokenPrefix token前缀, 格式样例(satoken: Bearer xxxx-xxxx-xxxx-xxxx)
     * @return 对象自身
     */
    public TokenConfig setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
        return this;
    }

    /**
     * @return 是否打印操作日志
     */
    public Boolean getIsLog() {
        return isLog;
    }

    /**
     * @param isLog 是否打印操作日志
     * @return 对象自身
     */
    public TokenConfig setIsLog(Boolean isLog) {
        this.isLog = isLog;
        return this;
    }

    /**
     * @return jwt秘钥 (只有集成 jwt 模块时此参数才会生效)
     */
    public String getJwtSecretKey() {
        return jwtSecretKey;
    }

    /**
     * @param jwtSecretKey jwt秘钥 (只有集成 jwt 模块时此参数才会生效)
     * @return 对象自身
     */
    public TokenConfig setJwtSecretKey(String jwtSecretKey) {
        this.jwtSecretKey = jwtSecretKey;
        return this;
    }

    /**
     * @return Id-Token的有效期 (单位: 秒)
     */
    public long getIdTokenTimeout() {
        return idTokenTimeout;
    }

    /**
     * @param idTokenTimeout Id-Token的有效期 (单位: 秒)
     * @return 对象自身
     */
    public TokenConfig setIdTokenTimeout(long idTokenTimeout) {
        this.idTokenTimeout = idTokenTimeout;
        return this;
    }

    /**
     * @return 是否校验Id-Token（部分rpc插件有效）
     */
    public Boolean getCheckIdToken() {
        return checkIdToken;
    }

    /**
     * @param checkIdToken 是否校验Id-Token（部分rpc插件有效）
     * @return 对象自身
     */
    public TokenConfig setCheckIdToken(Boolean checkIdToken) {
        this.checkIdToken = checkIdToken;
        return this;
    }

    /**
     * @return Cookie 全局配置对象
     */
    public CookieConfig getCookie() {
        return cookie;
    }

    /**
     * @param cookie Cookie 全局配置对象
     * @return 对象自身
     */
    public TokenConfig setCookie(CookieConfig cookie) {
        this.cookie = cookie;
        return this;
    }

    @Override
    public String toString() {
        return "TokenConfig{" +
                "tokenName='" + tokenName + '\'' +
                ", timeout=" + timeout +
                ", activityTimeout=" + activityTimeout +
                ", isConcurrent=" + isConcurrent +
                ", isShare=" + isShare +
                ", tokenStyle='" + tokenStyle + '\'' +
                ", dataRefreshPeriod=" + dataRefreshPeriod +
                ", tokenSessionCheckLogin=" + tokenSessionCheckLogin +
                ", autoRenew=" + autoRenew +
                ", tokenPrefix='" + tokenPrefix + '\'' +
                ", isLog=" + isLog +
                ", jwtSecretKey='" + jwtSecretKey + '\'' +
                ", idTokenTimeout=" + idTokenTimeout +
                ", checkIdToken=" + checkIdToken +
                ", cookie=" + cookie +
                '}';
    }
}
