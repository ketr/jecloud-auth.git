/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.strategy;

import com.je.auth.AuthEngine;
import com.je.auth.check.util.FoxUtil;
import com.je.auth.TokenConsts;
import com.je.common.auth.token.AuthSession;
import java.util.UUID;
import java.util.function.Function;

/**
 * Sa-Token 策略对象
 * <p>
 * 此类统一定义框架内的一些关键性逻辑算法，方便开发者进行按需重写，例：
 * </p>
 * <pre>
 * // SaStrategy全局单例，所有方法都用以下形式重写
 * SaStrategy.me.setCreateToken((loginId, loginType) -》 {
 * // 自定义Token生成的算法
 * return "xxxx";
 * });
 * </pre>
 *
 * @author kong
 */
public final class AuthStrategy {

    private AuthEngine authEngine;

    public AuthStrategy(AuthEngine authEngine) {
        this.authEngine = authEngine;
    }

    /**
     * 创建 Token 的策略
     * <p> 参数 [账号id, 账号类型]
     */
    public Function<Object, String> createToken = loginId -> {
        // 根据配置的tokenStyle生成不同风格的token
        String tokenStyle = authEngine.getConfig().getTokenStyle();
        // uuid
        if (TokenConsts.TOKEN_STYLE_UUID.equals(tokenStyle)) {
            return UUID.randomUUID().toString();
        }
        // 简单uuid (不带下划线)
        if (TokenConsts.TOKEN_STYLE_SIMPLE_UUID.equals(tokenStyle)) {
            return UUID.randomUUID().toString().replaceAll("-", "");
        }
        // 32位随机字符串
        if (TokenConsts.TOKEN_STYLE_RANDOM_32.equals(tokenStyle)) {
            return FoxUtil.getRandomString(32);
        }
        // 64位随机字符串
        if (TokenConsts.TOKEN_STYLE_RANDOM_64.equals(tokenStyle)) {
            return FoxUtil.getRandomString(64);
        }
        // 128位随机字符串
        if (TokenConsts.TOKEN_STYLE_RANDOM_128.equals(tokenStyle)) {
            return FoxUtil.getRandomString(128);
        }
        // tik风格 (2_14_16)
        if (TokenConsts.TOKEN_STYLE_TIK.equals(tokenStyle)) {
            return FoxUtil.getRandomString(2) + "_" + FoxUtil.getRandomString(14) + "_" + FoxUtil.getRandomString(16) + "__";
        }
        // 默认，还是uuid
        return UUID.randomUUID().toString();
    };

    /**
     * 创建 Session 的策略
     * <p> 参数 [SessionId]
     */
    public Function<String, AuthSession> createSession = (sessionId) -> {
        return authEngine.getAuthSessionTemplate().createSession(sessionId);
    };

    //
    // 重写策略 set连缀风格
    //

    /**
     * 重写创建 Token 的策略
     * <p> 参数 [账号id, 账号类型]
     *
     * @param createToken /
     * @return 对象自身
     */
    public AuthStrategy setCreateToken(Function<Object, String> createToken) {
        this.createToken = createToken;
        return this;
    }

    /**
     * 重写创建 Session 的策略
     * <p> 参数 [SessionId]
     *
     * @param createSession /
     * @return 对象自身
     */
    public AuthStrategy setCreateSession(Function<String, AuthSession> createSession) {
        this.createSession = createSession;
        return this;
    }


}
