/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.common.auth.token.AuthSession;

/**
 * 自定义session操作
 */
public class AuthSessionCustomTemplate {

    /**
     * 添加上指定前缀，防止恶意伪造Session
     */
    public static final String SESSION_KEY = "custom";

    private AuthEngine authEngine;

    public AuthEngine getAuthEngine() {
        return authEngine;
    }

    public void setAuthEngine(AuthEngine authEngine) {
        this.authEngine = authEngine;
    }

    /**
     * 拼接Key: 自定义Session的Id
     *
     * @param sessionId 会话id
     * @return sessionId
     */
    public String splicingSessionKey(String sessionId) {
        return authEngine.getConfig().getTokenName() + ":" + SESSION_KEY + ":session:" + sessionId;
    }

    /**
     * 指定key的Session是否存在
     *
     * @param sessionId Session的id
     * @return 是否存在
     */
    public boolean isExists(String sessionId) {
        return authEngine.getAuthTokenDao().getSession(splicingSessionKey(sessionId)) != null;
    }

    /**
     * 获取指定key的Session
     *
     * @param sessionId key
     * @param isCreate  如果此Session尚未在DB创建，是否新建并返回
     * @return SaSession
     */
    public AuthSession getSessionById(String sessionId, boolean isCreate) {
        AuthSession session = authEngine.getAuthTokenDao().getSession(splicingSessionKey(sessionId));
        if (session == null && isCreate) {
            session = authEngine.getAuthStrategy().createSession.apply(splicingSessionKey(sessionId));
            authEngine.getAuthTokenDao().setSession(session, authEngine.getConfig().getTimeout());
        }
        return session;
    }

    /**
     * 获取指定key的Session, 如果此Session尚未在DB创建，则新建并返回
     *
     * @param sessionId key
     * @return session对象
     */
    public AuthSession getSessionById(String sessionId) {
        return getSessionById(sessionId, true);
    }

    /**
     * 删除指定key的Session
     *
     * @param sessionId 指定key
     */
    public void deleteSessionById(String sessionId) {
        authEngine.getAuthTokenDao().deleteSession(splicingSessionKey(sessionId));
    }

}
