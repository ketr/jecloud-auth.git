/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

/**
 * Token常量类
 */
public class TokenConsts {

	// =================== 常量key标记 ===================  
	
	/**
	 * 常量key标记: 如果token为本次请求新创建的，则以此字符串为key存储在当前request中 
	 */
	public static final String JUST_CREATED = "JUST_CREATED_"; 	

	/**
	 * 常量key标记: 如果token为本次请求新创建的，则以此字符串为key存储在当前request中（不拼接前缀，纯Token）
	 */
	public static final String JUST_CREATED_NOT_PREFIX = "JUST_CREATED_NOT_PREFIX_"; 	

	/**
	 * 常量key标记: 如果本次请求已经验证过[无操作过期], 则以此值存储在当前request中 
	 */
	public static final String TOKEN_ACTIVITY_TIMEOUT_CHECKED_KEY = "TOKEN_ACTIVITY_TIMEOUT_CHECKED_KEY_"; 	

	/**
	 * 常量key标记: 在登录时，默认使用的设备名称 
	 */
	public static final String DEFAULT_LOGIN_DEVICE = "default-device"; 
	
	/**
	 * 常量key标记: 在进行临时身份切换时使用的key
	 */
	public static final String SWITCH_TO_SAVE_KEY = "SWITCH_TO_SAVE_KEY";


	// =================== token-style 相关 ===================  
	
	/**
	 * Token风格: uuid 
	 */
	public static final String TOKEN_STYLE_UUID = "uuid"; 
	
	/**
	 * Token风格: 简单uuid (不带下划线) 
	 */
	public static final String TOKEN_STYLE_SIMPLE_UUID = "simple-uuid"; 
	
	/**
	 * Token风格: 32位随机字符串 
	 */
	public static final String TOKEN_STYLE_RANDOM_32 = "random-32"; 
	
	/**
	 * Token风格: 64位随机字符串 
	 */
	public static final String TOKEN_STYLE_RANDOM_64 = "random-64"; 
	
	/**
	 * Token风格: 128位随机字符串 
	 */
	public static final String TOKEN_STYLE_RANDOM_128 = "random-128"; 
	
	/**
	 * Token风格: tik风格 (2_14_16) 
	 */
	public static final String TOKEN_STYLE_TIK = "tik"; 

	
	// =================== 其它 ===================  

	/**
	 * 连接Token前缀和Token值的字符 
	 */
	public static final String TOKEN_CONNECTOR_CHAT  = " "; 

	
}
