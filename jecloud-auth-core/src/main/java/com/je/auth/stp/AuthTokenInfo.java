/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.stp;

/**
 * Token信息Model: 用来描述一个Token的常用参数
 *
 * @author kong
 */
public class AuthTokenInfo {

    /**
     * token名称
     */
    public String tokenName;

    /**
     * token值
     */
    public String tokenValue;

    /**
     * 此token是否已经登录
     */
    public Boolean isLogin;

    /**
     * 此token对应的LoginId，未登录时为null
     */
    public Object loginId;

    /**
     * token剩余有效期 (单位: 秒)
     */
    public long tokenTimeout;

    /**
     * User-Session剩余有效时间 (单位: 秒)
     */
    public long sessionTimeout;

    /**
     * Token-Session剩余有效时间 (单位: 秒)
     */
    public long tokenSessionTimeout;

    /**
     * token剩余无操作有效时间 (单位: 秒)
     */
    public long tokenActivityTimeout;

    /**
     * 登录设备标识
     */
    public String loginDevice;

    /**
     * 自定义数据
     */
    public String tag;


    /**
     * @return token名称
     */
    public String getTokenName() {
        return tokenName;
    }

    /**
     * @param tokenName token名称
     */
    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    /**
     * @return token值
     */
    public String getTokenValue() {
        return tokenValue;
    }

    /**
     * @param tokenValue token值
     */
    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    /**
     * @return 此token是否已经登录
     */
    public Boolean getIsLogin() {
        return isLogin;
    }

    /**
     * @param isLogin 此token是否已经登录
     */
    public void setIsLogin(Boolean isLogin) {
        this.isLogin = isLogin;
    }

    /**
     * @return 此token对应的LoginId，未登录时为null
     */
    public Object getLoginId() {
        return loginId;
    }

    /**
     * @param loginId 此token对应的LoginId，未登录时为null
     */
    public void setLoginId(Object loginId) {
        this.loginId = loginId;
    }

    /**
     * @return token剩余有效期 (单位: 秒)
     */
    public long getTokenTimeout() {
        return tokenTimeout;
    }

    /**
     * @param tokenTimeout token剩余有效期 (单位: 秒)
     */
    public void setTokenTimeout(long tokenTimeout) {
        this.tokenTimeout = tokenTimeout;
    }

    /**
     * @return User-Session剩余有效时间 (单位: 秒)
     */
    public long getSessionTimeout() {
        return sessionTimeout;
    }

    /**
     * @param sessionTimeout User-Session剩余有效时间 (单位: 秒)
     */
    public void setSessionTimeout(long sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    /**
     * @return Token-Session剩余有效时间 (单位: 秒)
     */
    public long getTokenSessionTimeout() {
        return tokenSessionTimeout;
    }

    /**
     * @param tokenSessionTimeout Token-Session剩余有效时间 (单位: 秒)
     */
    public void setTokenSessionTimeout(long tokenSessionTimeout) {
        this.tokenSessionTimeout = tokenSessionTimeout;
    }

    /**
     * @return token剩余无操作有效时间 (单位: 秒)
     */
    public long getTokenActivityTimeout() {
        return tokenActivityTimeout;
    }

    /**
     * @param tokenActivityTimeout token剩余无操作有效时间 (单位: 秒)
     */
    public void setTokenActivityTimeout(long tokenActivityTimeout) {
        this.tokenActivityTimeout = tokenActivityTimeout;
    }

    /**
     * @return 登录设备标识
     */
    public String getLoginDevice() {
        return loginDevice;
    }

    /**
     * @param loginDevice 登录设备标识
     */
    public void setLoginDevice(String loginDevice) {
        this.loginDevice = loginDevice;
    }

    /**
     * @return 自定义数据
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag 自定义数据
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "AuthTokenInfo{" +
                "tokenName='" + tokenName + '\'' +
                ", tokenValue='" + tokenValue + '\'' +
                ", isLogin=" + isLogin +
                ", loginId=" + loginId +
                ", tokenTimeout=" + tokenTimeout +
                ", sessionTimeout=" + sessionTimeout +
                ", tokenSessionTimeout=" + tokenSessionTimeout +
                ", tokenActivityTimeout=" + tokenActivityTimeout +
                ", loginDevice='" + loginDevice + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }
}
