/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.stp;

import com.je.auth.AuthTokenDao;
import com.je.auth.config.TokenConfig;
import com.je.auth.TokenConsts;

import java.util.LinkedHashMap;
import java.util.Map;

public class AuthLoginModel {

    /**
     * 此次登录的客户端设备标识
     */
    public String device;
    /**
     * 是否为持久Cookie（临时Cookie在浏览器关闭时会自动删除，持久Cookie在重新打开后依然存在）
     */
    public Boolean isLastingCookie;
    /**
     * 指定此次登录token的有效期, 单位:秒 （如未指定，自动取全局配置的timeout值）
     */
    public Long timeout;
    /**
     * 扩展信息（只在jwt模式下生效）
     */
    public Map<String, Object> extraData;
    /**
     * 预定Token（预定本次登录生成的Token值）
     */
    public String token;

    /**
     * @return 此次登录的客户端设备标识
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device 此次登录的客户端设备标识
     * @return 对象自身
     */
    public AuthLoginModel setDevice(String device) {
        this.device = device;
        return this;
    }

    /**
     * @return 参考 是否为持久Cookie（临时Cookie在浏览器关闭时会自动删除，持久Cookie在重新打开后依然存在）
     */
    public Boolean getIsLastingCookie() {
        return isLastingCookie;
    }

    /**
     * @param isLastingCookie 是否为持久Cookie（临时Cookie在浏览器关闭时会自动删除，持久Cookie在重新打开后依然存在）
     * @return 对象自身
     */
    public AuthLoginModel setIsLastingCookie(Boolean isLastingCookie) {
        this.isLastingCookie = isLastingCookie;
        return this;
    }

    /**
     * @return 指定此次登录token的有效期, 单位:秒 （如未指定，自动取全局配置的timeout值）
     */
    public Long getTimeout() {
        return timeout;
    }

    /**
     * @param timeout 指定此次登录token的有效期, 单位:秒 （如未指定，自动取全局配置的timeout值）
     * @return 对象自身
     */
    public AuthLoginModel setTimeout(long timeout) {
        this.timeout = timeout;
        return this;
    }

    public void configBuild(TokenConfig tokenConfig) {
        if (isLastingCookie == null) {
            isLastingCookie = true;
        }
        if (timeout == null) {
            timeout = tokenConfig.getTimeout();
        }
    }

    /**
     * @return 扩展信息（只在jwt模式下生效）
     */
    public Map<String, Object> getExtraData() {
        return extraData;
    }

    /**
     * @param extraData 扩展信息（只在jwt模式下生效）
     * @return 对象自身
     */
    public AuthLoginModel setExtraData(Map<String, Object> extraData) {
        this.extraData = extraData;
        return this;
    }

    /**
     * @return 预定Token（预定本次登录生成的Token值）
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token 预定Token（预定本次登录生成的Token值）
     * @return 对象自身
     */
    public AuthLoginModel setToken(String token) {
        this.token = token;
        return this;
    }

    @Override
    public String toString() {
        return "AuthLoginModel{" +
                "device='" + device + '\'' +
                ", isLastingCookie=" + isLastingCookie +
                ", timeout=" + timeout +
                ", extraData=" + extraData +
                ", token='" + token + '\'' +
                '}';
    }

    // ------ 附加方法


    /**
     * 写入扩展数据（只在jwt模式下生效）
     *
     * @param key   键
     * @param value 值
     * @return 对象自身
     */
    public AuthLoginModel setExtra(String key, Object value) {
        if (this.extraData == null) {
            this.extraData = new LinkedHashMap<>();
        }
        this.extraData.put(key, value);
        return this;
    }

    /**
     * 获取扩展数据（只在jwt模式下生效）
     *
     * @param key 键
     * @return 扩展数据的值
     */
    public Object getExtra(String key) {
        if (this.extraData == null) {
            return null;
        }
        return this.extraData.get(key);
    }

    /**
     * @return Cookie时长
     */
    public int getCookieTimeout() {
        if (isLastingCookie == false) {
            return -1;
        }
        if (timeout == AuthTokenDao.NEVER_EXPIRE) {
            return Integer.MAX_VALUE;
        }
        return (int) (long) timeout;
    }

    /**
     * @return 获取device参数，如果为null，则返回默认值
     */
    public String getDeviceOrDefault() {
        if (device == null) {
            return TokenConsts.DEFAULT_LOGIN_DEVICE;
        }
        return device;
    }

}
