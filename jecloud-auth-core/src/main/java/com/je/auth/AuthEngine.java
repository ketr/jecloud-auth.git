/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.check.AuthCheckEngine;
import com.je.auth.config.TokenConfig;
import com.je.auth.impl.AuthTokenDaoDefaultImpl;
import com.je.auth.listener.AuthEvent;
import com.je.auth.listener.AuthTokenListener;
import com.je.auth.listener.AuthTokenListenerDefaultImpl;
import com.je.auth.strategy.AuthStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 * 认证engine实现，作为统一初始化、统一配置与门面
 */
public class AuthEngine {

    /**
     * check引擎
     */
    private AuthCheckEngine authCheckEngine;
    /**
     * 登录策略
     */
    private AuthStrategy authStrategy = new AuthStrategy(this);
    /**
     * Token 侦听器
     */
    private List<AuthTokenListener> listeners = new ArrayList<>();
    /**
     * 配置文件 Bean
     */
    private TokenConfig config;
    /**
     * 登录管理器
     */
    private AuthLoginManager authLoginManager;
    /**
     * Token持久层接口
     */
    private AuthTokenDao authTokenDao;
    /**
     * 临时令牌验证模块接口
     */
    private AuthTempInterface authTempInterface;
    /**
     * session管理器
     */
    private AuthSessionTemplate authSessionTemplate;
    /**
     * 自定义session管理器
     */
    private AuthSessionCustomTemplate authSessionCustomTemplate;

    public AuthEngine(TokenConfig config,AuthCheckEngine authCheckEngine,
                      AuthLoginManager authLoginManager,
                      AuthTokenDao authTokenDao,
                      AuthTempInterface authTempInterface,
                      AuthSessionTemplate authSessionTemplate,
                      AuthSessionCustomTemplate authSessionCustomTemplate) {
        this.config = config;
        this.authCheckEngine = authCheckEngine;
        this.authLoginManager = authLoginManager;
        this.authLoginManager.setAuthEngine(this);

        this.authTokenDao = authTokenDao;
        this.authTokenDao.setAuthEngine(this);

        this.authTempInterface = authTempInterface;
        this.authTempInterface.setAuthEngine(this);

        this.authSessionTemplate = authSessionTemplate;
        this.authSessionTemplate.setAuthEngine(this);
        this.authSessionCustomTemplate = authSessionCustomTemplate;
        this.authSessionCustomTemplate.setAuthEngine(this);
        this.init();
    }

    public AuthCheckEngine getAuthCheckEngine() {
        return authCheckEngine;
    }

    public void setAuthCheckEngine(AuthCheckEngine authCheckEngine) {
        this.authCheckEngine = authCheckEngine;
    }

    public AuthLoginManager getAuthLoginManager() {
        return authLoginManager;
    }

    public void setAuthLoginManager(AuthLoginManager authLoginManager) {
        this.authLoginManager = authLoginManager;
    }

    public TokenConfig getConfig() {
        return config;
    }

    public void setConfig(TokenConfig config) {
        this.config = config;
    }

    public AuthTokenDao getAuthTokenDao() {
        return authTokenDao;
    }

    public void setAuthTokenDao(AuthTokenDao authTokenDao) {
        this.authTokenDao = authTokenDao;
        if ((this.authTokenDao instanceof AuthTokenDaoDefaultImpl)) {
            ((AuthTokenDaoDefaultImpl) this.authTokenDao).endRefreshThread();
        }
    }

    public List<AuthTokenListener> getListeners() {
        return listeners;
    }

    public AuthTempInterface getAuthTempInterface() {
        return authTempInterface;
    }

    public void setAuthTempInterface(AuthTempInterface authTempInterface) {
        this.authTempInterface = authTempInterface;
    }

    public AuthStrategy getAuthStrategy() {
        return authStrategy;
    }

    public AuthSessionTemplate getAuthSessionTemplate() {
        return authSessionTemplate;
    }

    public void setAuthSessionTemplate(AuthSessionTemplate authSessionTemplate) {
        this.authSessionTemplate = authSessionTemplate;
    }

    public AuthSessionCustomTemplate getAuthSessionCustomTemplate() {
        return authSessionCustomTemplate;
    }

    public void setAuthSessionCustomTemplate(AuthSessionCustomTemplate authSessionCustomTemplate) {
        this.authSessionCustomTemplate = authSessionCustomTemplate;
    }

    /**
     * 初始化
     */
    private void init() {
        //初始化监听，初始化增加默认，一般为日志提醒实现
        listeners.add(new AuthTokenListenerDefaultImpl(this));

        //如果是默认则调用
        if (authTokenDao instanceof AuthTokenDaoDefaultImpl) {
            ((AuthTokenDaoDefaultImpl) authTokenDao).initRefreshThread();
        }
    }

    /**
     * 增加认证监听
     *
     * @param authTokenListener
     */
    public void addListener(AuthTokenListener authTokenListener) {
        listeners.add(authTokenListener);
    }

    /**
     * 触发相关事件
     *
     * @param event
     */
    public void trigger(AuthEvent event) {
        for (AuthTokenListener eachListener : listeners) {
            eachListener.execute(event);
        }
    }

}
