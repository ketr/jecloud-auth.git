/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.listener;

import com.je.auth.AuthEngine;
import com.je.auth.stp.AuthLoginModel;
import com.je.auth.check.util.FoxUtil;
import java.util.Date;

/**
 * 侦听器的默认实现：log打印
 *
 * @author kong
 */
public class AuthTokenListenerDefaultImpl implements AuthTokenListener {

	private AuthEngine authEngine;

	public AuthTokenListenerDefaultImpl(AuthEngine authEngine) {
		this.authEngine = authEngine;
	}

	@Override
	public void execute(AuthEvent event) {
		if (AuthEvent.AuthEventType.LOGIN.equals(event.getEventType())) {
			doLogin(event.getLoginId(), event.getAuthLoginModel());
		}

		if (AuthEvent.AuthEventType.LOGOUT.equals(event.getEventType())) {
			doLogout(event.getLoginId(), event.getTokenValue());
		}

		if (AuthEvent.AuthEventType.KICKOUT.equals(event.getEventType())) {
			doKickout(event.getLoginId(), event.getTokenValue());
		}

		if (AuthEvent.AuthEventType.RELACED.equals(event.getEventType())) {
			doReplaced(event.getLoginId(), event.getTokenValue());
		}

		if (AuthEvent.AuthEventType.DISABLE.equals(event.getEventType())) {
			doDisable(event.getLoginId(), event.getDisableTime());
		}

		if (AuthEvent.AuthEventType.UNTIE_DISABLE.equals(event.getEventType())) {
			doUntieDisable(event.getLoginId());
		}

		if (AuthEvent.AuthEventType.CREATE_SESSION.equals(event.getEventType())) {
			doCreateSession(event.getSessionId());
		}

		if (AuthEvent.AuthEventType.LOGOUT_SESSION.equals(event.getEventType())) {
			doLogoutSession(event.getSessionId());
		}
	}

	/**
	 * 每次登录时触发
	 */
	public void doLogin(Object loginId, AuthLoginModel loginModel) {
		println("账号[" + loginId + "]登录成功");
	}

	/**
	 * 每次注销时触发
	 */
	public void doLogout(Object loginId, String tokenValue) {
		println("账号[" + loginId + "]注销成功 (Token=" + tokenValue + ")");
	}

	/**
	 * 每次被踢下线时触发
	 */
	public void doKickout(Object loginId, String tokenValue) {
		println("账号[" + loginId + "]被踢下线 (Token=" + tokenValue + ")");
	}

	/**
	 * 每次被顶下线时触发
	 */
	public void doReplaced(Object loginId, String tokenValue) {
		println("账号[" + loginId + "]被顶下线 (Token=" + tokenValue + ")");
	}

	/**
	 * 每次被封禁时触发
	 */
	public void doDisable(Object loginId, long disableTime) {
		Date date = new Date(System.currentTimeMillis() + disableTime * 1000);
		println("账号[" + loginId + "]被封禁 (解封时间: " + FoxUtil.formatDate(date) + ")");
	}

	/**
	 * 每次被解封时触发
	 */
	public void doUntieDisable(Object loginId) {
		println("账号[" + loginId + "]被解除封禁");
	}

	/**
	 * 每次创建Session时触发
	 */
	public void doCreateSession(String id) {
		println("Session[" + id + "]创建成功");
	}

	/**
	 * 每次注销Session时触发
	 */
	public void doLogoutSession(String id) {
		println("Session[" + id + "]注销成功");
	}

	/**
	 * 日志输出的前缀
	 */
	public static final String LOG_PREFIX = "AuthLog -->: ";
	
	/**
	 * 打印指定字符串
	 * @param str 字符串
	 */
	public void println(String str) {
		if (authEngine.getConfig().getIsLog()) {
			System.out.println(LOG_PREFIX + str);
		}
	}

}
