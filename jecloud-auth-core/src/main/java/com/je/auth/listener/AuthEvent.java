/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth.listener;

import com.je.auth.stp.AuthLoginModel;

import java.io.Serializable;

/**
 * 认证事件定义
 */
public class AuthEvent implements Serializable {

    /**
     * 事件类型
     */
    private AuthEventType eventType;
    /**
     * 账号Id
     */
    private Object loginId;
    /**
     * 登录参数
     */
    private AuthLoginModel authLoginModel;

    private String tokenValue;

    private long disableTime;

    private String sessionId;

    public AuthEvent() {
    }

    public AuthEvent(String loginId) {
        this.loginId = loginId;
    }

    public AuthEvent(String loginId, AuthLoginModel authLoginModel) {
        this.loginId = loginId;
        this.authLoginModel = authLoginModel;
    }

    public AuthEvent(AuthEventType eventType, String loginId, AuthLoginModel authLoginModel) {
        this.eventType = eventType;
        this.loginId = loginId;
        this.authLoginModel = authLoginModel;
    }

    public AuthEventType getEventType() {
        return eventType;
    }

    public AuthEvent setEventType(AuthEventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public Object getLoginId() {
        return loginId;
    }

    public AuthEvent setLoginId(Object loginId) {
        this.loginId = loginId;
        return this;
    }

    public AuthLoginModel getAuthLoginModel() {
        return authLoginModel;
    }

    public AuthEvent setAuthLoginModel(AuthLoginModel authLoginModel) {
        this.authLoginModel = authLoginModel;
        return this;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public AuthEvent setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
        return this;
    }

    public long getDisableTime() {
        return disableTime;
    }

    public AuthEvent setDisableTime(long disableTime) {
        this.disableTime = disableTime;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public AuthEvent setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public static AuthEvent build() {
        return new AuthEvent();
    }

    /**
     * 事件类型
     */
    public enum AuthEventType {
        /**
         * 登录
         */
        LOGIN,
        /**
         * 注销
         */
        LOGOUT,
        /**
         * 被踢下线
         */
        KICKOUT,
        /**
         * 被顶下线
         */
        RELACED,
        /**
         * 被封禁
         */
        DISABLE,
        /**
         * 被解封
         */
        UNTIE_DISABLE,
        /**
         * 创建session
         */
        CREATE_SESSION,
        /**
         * 注销session
         */
        LOGOUT_SESSION;
    }

}
