/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.auth;

import com.je.auth.check.util.FoxUtil;

/**
 * Token 临时令牌验证模块接口
 *
 * @author kong
 */
public interface AuthTempInterface {

    AuthEngine getAuthEngine();

    void setAuthEngine(AuthEngine authEngine);

    /**
     * 根据value创建一个token
     *
     * @param value   指定值
     * @param timeout 有效期，单位：秒
     * @return 生成的token
     */
    default String createToken(Object value, long timeout) {
        // 生成 token
        String token = getAuthEngine().getAuthStrategy().createToken.apply(null);

        // 持久化映射关系
        String key = splicingKeyTempToken(token);
        getAuthEngine().getAuthTokenDao().setObject(key, value, timeout);
        // 返回
        return token;
    }

    /**
     * 解析token获取value
     *
     * @param token 指定token
     * @return See Note
     */
    default Object parseToken(String token) {
        String key = splicingKeyTempToken(token);
        return getAuthEngine().getAuthTokenDao().getObject(key);
    }

    /**
     * 解析token获取value，并转换为指定类型
     *
     * @param token 指定token
     * @param cs    指定类型
     * @param <T>   默认值的类型
     * @return See Note
     */
    default <T> T parseToken(String token, Class<T> cs) {
        return FoxUtil.getValueByType(parseToken(token), cs);
    }

    /**
     * 获取指定 token 的剩余有效期，单位：秒
     * <p> 返回值 -1 代表永久，-2 代表token无效
     *
     * @param token see note
     * @return see note
     */
    default long getTimeout(String token) {
        String key = splicingKeyTempToken(token);
        return getAuthEngine().getAuthTokenDao().getObjectTimeout(key);
    }

    /**
     * 删除一个 token
     *
     * @param token 指定token
     */
    default void deleteToken(String token) {
        String key = splicingKeyTempToken(token);
        getAuthEngine().getAuthTokenDao().deleteObject(key);
    }

    /**
     * 获取映射关系的持久化key
     *
     * @param token token值
     * @return key
     */
    default String splicingKeyTempToken(String token) {
        return getAuthEngine().getConfig().getTokenName() + ":temp-token:" + token;
    }

    /**
     * @return jwt秘钥 (只有集成 sa-token-temp-jwt 模块时此参数才会生效)
     */
    default String getJwtSecretKey() {
        return null;
    }

}
