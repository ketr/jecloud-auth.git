# 认证项目

## 项目简介

JECloud基础认证模块，用于为JECloud提供统一的登录认证及鉴权机制。

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。

> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- jecloud-auth-check： 登录及权限校验模块
- jecloud-auth-core: 登录及权限核心模块
- jecloud-auth-plugin: 登录插件模块
- jecloud-auth-starter: springboot starter模块
- jecloud-auth-test: 测试模块
- jecloud-auth-third-app: 三方app集成模块
- jecloud-auth-third-login: 三方登录集成模块

## 编译部署

``` shell
mvn clean package -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)