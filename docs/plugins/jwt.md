# JWT集成

## 集成方式

```
<dependency>
    <groupId>com.je.auth</groupId>
    <artifactId>jecloud-auth-token-jwt</artifactId>
    <version>${project.version}</version>
</dependency>
```

### 1. 配置秘钥

```yaml

jecloud:
  auth:
    # jwt秘钥
    jwt-secret-key: asdasdasifhueuiwyurfewbfjsdafjk
```

## 2. 注入AuthManager

> 注意：只能注入一种风格的认证管理器。

### style风格jwt

```java

@Bean("styleJwtAuthManager")
public AuthManager styleJwtAuthManager(){
        return new StyleJwtAuthManagerImpl();
        }

```

### mix模式风格jwt

```java

@Bean("statelessJwtAuthManager")
public AuthManager statelessJwtAuthManager(){
        return new StatelessJwtAuthManagerImpl();
        }

```

### staless模式风格jwt

```java
    @Bean("mixJwtAuthManager")
public AuthManager authManager(){
        return new MixJwtManagerImpl();
        }
```

### 使用

```java

@Test
public void doLogin(){
        // 登录
        authManager.login(10001);
        String token=authManager.getTokenValue();

        // API 验证
        Assert.assertTrue(authManager.isLogin());
        Assert.assertNotNull(token);    // token不为null
        Assert.assertEquals(authManager.getLoginIdAsLong(),10001);    // loginId=10001
        Assert.assertEquals(authManager.getLoginDevice(),TokenConsts.DEFAULT_LOGIN_DEVICE);    // 登录设备

        // token 验证
        JWT jwt=JWT.of(token);
        JSONObject payloads=jwt.getPayloads();
        Assert.assertEquals(payloads.getStr(JwtUtil.LOGIN_ID),"10001"); // 账号
        Assert.assertEquals(payloads.getStr(JwtUtil.DEVICE),TokenConsts.DEFAULT_LOGIN_DEVICE);  // 登录设备

        // db数据 验证
        // token不存在
        Assert.assertNull(dao.get("authorization:token:"+token));
        // Session 存在
        AuthSession session=dao.getSession("authorization:session:"+10001);
        Assert.assertNotNull(session);
        Assert.assertEquals(session.getId(),"authorization:session:"+10001);
        Assert.assertTrue(session.getTokenSignList().size()>=1);
        }

```

## 不同模式策略对比

注入不同模式会让框架具有不同的行为策略，以下是三种模式的差异点（为方便叙述，以下比较以同时引入 jwt 与 Redis 作为前提）：

| 功能点                        | Style 模式        | Mix 模式            | Stateless 模式    |
| :--------					| :--------		| :--------			| :--------			|
| Token风格                    | jwt风格        | jwt风格            | jwt风格            |
| 登录数据存储                | Redis中        | Token中            | Token中            |
| Session存储                | Redis中        | Redis中            | 无Session            |
| 注销下线                    | 前后端双清数据    | 前后端双清数据        | 前端清除数据        |
| 踢人下线API                | 支持            | 不支持                | 不支持                |
| 登录认证                    | 支持            | 支持                | 支持                |
| 角色认证                    | 支持            | 支持                | 支持                |
| 权限认证                    | 支持            | 支持                | 支持                |
| timeout 有效期                | 支持            | 支持                | 支持                |
| activity-timeout 有效期    | 支持            | 支持                | 不支持                |
| id反查Token                | 支持            | 支持                | 不支持                |
| 会话管理                    | 支持            | 部分支持            | 不支持                |
| 注解鉴权                    | 支持            | 支持                | 支持                |
| 账号封禁                    | 支持            | 支持                | 不支持                |
| 身份切换                    | 支持            | 支持                | 支持                |
| 二级认证                    | 支持            | 支持                | 支持                |
| 模式总结                    | Token风格替换    | jwt 与 Redis 逻辑混合    | 完全舍弃Redis，只用jwt        |