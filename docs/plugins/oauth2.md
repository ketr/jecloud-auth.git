# OAUTH2.0 实现

OAuth2.0实现主要包括以下集中实现：

1. 授权码（Authorization Code）：OAuth2.0标准授权步骤，Server端向Client端下放Code码，Client端再用Code码换取授权Token。
2. 隐藏式（Implicit）：无法使用授权码模式时的备用选择，Server端使用URL重定向方式直接将Token下放到Client端页面。
3. 密码式（Password）：Client直接拿着用户的账号密码换取授权Token。
4. 客户端凭证（Client Credentials）：Server端针对Client级别的Token，代表应用自身的资源授权。

## 授权码（Authorization Code）

1. 跳转认证：第三方应用跳转到服务端url：/oauth2/authorize进行授权。
2. 服务端发现用户没有登录，则直接跳转到loginView。
3. 用户登录完成，服务端跳转到/oauth2/authorize。
4. 服务端发现用户需要访问受限制资源，跳转到confirmView。
5. 用户同意授权，服务端则跳转到url：/oauth2/authorize，获取到授权码（code），并重定向到用户地址。
6. 第三方系统获取到授权码（code），根据授权码获取access_token和refresh_token。
7. 第三方系统重定向到系统首页。

## 隐藏式（Implicit）

## 密码式（Password）

## 客户端凭证（Client Credentials）